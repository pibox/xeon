# ---------------------------------------------------------------
# Build the root filesystem via Busybox
# ---------------------------------------------------------------
.$(BUSYBOX_T)-validate $(BUSYBOX_T)-validate:
	@if [ ! -d $(KERNEL_SRCDIR)/modules ]; then \
		$(MSG3) ${KERNEL_T} component must be built before ${BUSYBOX_T} $(EMSG); \
	fi
	@touch .$(subst .,,$@)

.$(BUSYBOX_T)-get $(BUSYBOX_T)-get:
	@mkdir -p $(BLDDIR) $(BUSYBOX_ARCDIR)
	@if [ ! -f $(BUSYBOX_ARCDIR)/$(BUSYBOX_VERSION).tar.bz2 ]; then \
		$(MSG) "================================================================"; \
		$(MSG3) Retrieving Busybox package $(EMSG); \
		$(MSG) "================================================================"; \
		D=$(BUSYBOX_ARCDIR) S=$(BUSYBOX_PKG_NAME) U=$(BUSYBOX_URL) make --no-print-directory getsw-only; \
	else \
		$(MSG3) Busybox package is cached $(EMSG); \
	fi
	@touch .$(subst .,,$@)

.$(BUSYBOX_T)-unpack $(BUSYBOX_T)-unpack: .$(BUSYBOX_T)-get
	@$(MSG3) "Unpacking Busybox" $(EMSG)
	@mkdir -p $(BUSYBOX_BLDDIR) $(BUSYBOX_ARCDIR)
	@if [ ! -d $(BUSYBOX_SRCDIR) ]; then \
		tar -C $(BLDDIR) -$(BUSYBOX_JZ)xf $(BUSYBOX_ARCDIR)/$(BUSYBOX_PKG_NAME); \
	fi
	@touch .$(subst .,,$@)

.$(BUSYBOX_T)-patch $(BUSYBOX_T)-patch: .$(BUSYBOX_T)-unpack
	@if [ -d $(BUSYBOX_PATCHDIR) ]; then \
		for patchname in `ls -1 $(BUSYBOX_PATCHDIR)`; do \
			$(MSG3) Applying $$patchname $(EMSG); \
			cd $(BUSYBOX_SRCDIR) && patch -Np1 -r - < $(BUSYBOX_PATCHDIR)/$$patchname 2>&1 >/dev/null ; \
		done; \
	fi
	@touch .$(subst .,,$@)

# Setup for the root filesystem build
.$(BUSYBOX_T)-init $(BUSYBOX_T)-init: .$(BUSYBOX_T)-patch 
	@touch .$(subst .,,$@)

$(BUSYBOX_T)-preconfig: 
	@if [ -f $(BUSYBOX_INITRD) ]; then \
		$(MSG3) "Using local config" $(EMSG); \
		cp $(BUSYBOX_INITRD) $(BUSYBOX_SRCDIR)/.config; \
		cd $(BUSYBOX_SRCDIR) && PATH=$(BUSYBOX_PATH) \
			make -j$(JOBS) ARCH=$(ARCH) CROSS_COMPILE=$(CROSS_COMPILER) oldconfig; \
	else \
		$(MSG6) "No local config - using defconfig" $(EMSG); \
		cd $(BUSYBOX_SRCDIR) && PATH=$(BUSYBOX_PATH) \
			make -j$(JOBS) ARCH=$(ARCH) CROSS_COMPILE=$(CROSS_COMPILER) $(BUSYBOX_DEFCONFIG); \
	fi

$(BUSYBOX_T)-config: 
	@cp $(BUSYBOX_INITRD) $(BUSYBOX_SRCDIR)/.config
	@sed -i 's%\[BLDDIR\]%$(BUSYBOX_BLDDIR)%g' $(BUSYBOX_SRCDIR)/.config

.$(BUSYBOX_T) $(BUSYBOX_T): .$(XCC_T) .$(BUSYBOX_T)-init
	@$(MSG) "================================================================"
	@$(MSG2) "Building BUSYBOX" $(EMSG)
	@$(MSG) "================================================================"
	@mkdir -p $(BUSYBOX_BLDDIR)/images $(BUSYBOX_ARCDIR)
	@make $(BUSYBOX_T)-config
	@cd $(BUSYBOX_SRCDIR) && make oldconfig
	@make $(BUSYBOX_T)-preconfig
	@$(MSG3) "Running BUSYBOX build" $(EMSG)
	@cd $(BUSYBOX_SRCDIR) && PATH=$(BUSYBOX_PATH) \
		make CONFIG_PREFIX=$(BUSYBOX_BLDDIR)/busybox CROSS_COMPILE=$(CROSS_COMPILER)
	@cd $(BUSYBOX_SRCDIR) && PATH=$(BUSYBOX_PATH) \
		make CONFIG_PREFIX=$(BUSYBOX_BLDDIR)/busybox CROSS_COMPILE=$(CROSS_COMPILER) install
	@if [ -d $(BUSYBOX_BLDDIR)/busybox ]; then \
		$(MSG6) "Built root filesystem:" $(EMSG); \
	else \
		$(MSG3) "Busybox completed but can't find root filesystem:" $(EMSG); \
	fi
	@$(MSG3) "Fixing permission" $(EMSG)
	@ls -l $(BUSYBOX_BLDDIR)/busybox
	@touch .$(subst .,,$@)

$(BUSYBOX_T)-menuconfig: $(BUSYBOX_T)-config
	@cd $(BUSYBOX_SRCDIR) && make BUSYBOX_CONFIG=$(BUSYBOX_SRCDIR)/.busybox_config menuconfig

$(BUSYBOX_T)-meld:
	@meld $(BUSYBOX_SRCDIR)/.config $(BUSYBOX_INITRD)

$(BUSYBOX_T)-saveconfig:
	@$(MSG) "================================================================"
	@$(MSG2) "Saving Busybox configuration" $(EMSG)
	@$(MSG) "================================================================"
	@cp $(BUSYBOX_SRCDIR)/.config $(BUSYBOX_INITRD) 
	@sed -i 's%$(BUSYBOX_BLDDIR)%\[BLDDIR\]%g' $(BUSYBOX_INITRD)

# This is a no-op because we don't use the artifacts from a busybox build.
# These targets are only for testing configurations of busybox for use with buildroot.
$(BUSYBOX_T)-files:

# Build an ext3 filesystem from the buildroot package
# Add the RPi Userland tools manually at this point.
$(BUSYBOX_T)-ext3:
	mkdir -p $(BUSYBOX_BLDDIR)/images
	mkdir -p $(BUSYBOX_BLDDIR)/mnt
	dd if=/dev/zero of=$(BUSYBOX_BLDDIR)/images/rootfs.ext3 bs=1M count=3
	mke2fs -F -j $(BUSYBOX_BLDDIR)/images/rootfs.ext3
	sudo mount -o loop $(BUSYBOX_BLDDIR)/images/rootfs.ext3 $(BUSYBOX_BLDDIR)/mnt
	sudo rsync -av $(BUSYBOX_BLDDIR)/busybox/ $(BUSYBOX_BLDDIR)/mnt/
	sudo chown -R root.root $(BUSYBOX_BLDDIR)/mnt/*
	sudo umount $(BUSYBOX_BLDDIR)/mnt

$(BUSYBOX_T)-initramfs:
	mkdir -p $(BUSYBOX_BLDDIR)/images
	cp $(DIR_BUSYBOX)/init $(BUSYBOX_BLDDIR)/busybox
	cd $(KERNEL_MODDIR) && ls -1 > $(BUSYBOX_KV)
	mkdir -p $(BUSYBOX_MODDIR)/`cat $(BUSYBOX_KV)`/kernel/fs/squashfs \
		$(BUSYBOX_MODDIR)/`cat $(BUSYBOX_KV)`/kernel/fs/overlay
	cd $(KERNEL_MODDIR)/ && \
		find . -name "squashfs.ko" \
			-exec cp {} $(BUSYBOX_MODDIR)/`cat $(BUSYBOX_KV)`/kernel/fs/squashfs/ \; && \
		find . -name "overlay.ko" \
			-exec cp {} $(BUSYBOX_MODDIR)/`cat $(BUSYBOX_KV)`/kernel/fs/overlay/ \;
	rm -f $(BUSYBOX_KV)
	rm -f $(BUSYBOX_BLDDIR)/busybox/linuxrc
	cd $(BUSYBOX_BLDDIR)/busybox/ && find . | \
		cpio -H newc -o | gzip -9 > $(BUSYBOX_BLDDIR)/images/initramfs.gz
	rm -f $(BUSYBOX_BLDDIR)/busybox/init
	ln -sf /bin/busybox $(BUSYBOX_BLDDIR)/busybox/linuxrc

# This is a no-op because we don't use the artifacts from a busybox build.
# These targets are only for testing configurations of busybox for use with buildroot.
$(BUSYBOX_T)-pkg: .$(BUSYBOX_T)
	@$(MSG) "================================================================"
	@$(MSG2) "Gathering for $(BUSYBOX_T)" $(EMSG)
	@$(MSG) "================================================================"
	@$(MSG3) "Generating ext3 rootfs image" $(EMSG)
	@make --no-print-directory -i $(BUSYBOX_T)-ext3
	@$(MSG3) "Generating initramfs image" $(EMSG)
	@make --no-print-directory -i $(BUSYBOX_T)-initramfs
	@$(MSG3) "Grabbing $(BUSYBOX_T) ext3 image" $(EMSG)
	@if [ -f $(BUSYBOX_BLDDIR)/images/rootfs.ext3 ]; then \
		cp $(BUSYBOX_BLDDIR)/images/rootfs.ext3 $(PKGDIR)/busybox.ext3; \
		ls -l $(PKGDIR)/busybox.ext3; \
	else \
		$(MSG11) "Missing ext3 rootfs image: $(BUSYBOX_BLDDIR)/images/rootfs.ext3" $(EMSG); \
	fi
	@if [ -f $(BUSYBOX_BLDDIR)/images/initramfs.gz ]; then \
		cp $(BUSYBOX_BLDDIR)/images/initramfs.gz $(PKGDIR)/; \
		ls -l $(PKGDIR)/initramfs.gz; \
	else \
		$(MSG11) "Missing initramfs image: $(BUSYBOX_BLDDIR)/images/initramfs.gz" $(EMSG); \
	fi

$(BUSYBOX_T)-clean:
	@if [ -d "$(BUSYBOX_SRCDIR)" ]; then cd $(BUSYBOX_SRCDIR) && make -i clean; fi
	@sudo rm -rf $(BUSYBOX_BLDDIR) 
	@rm -f .$(BUSYBOX_T) 

$(BUSYBOX_T)-clobber: 
	@if [ -d "$(BUSYBOX_SRCDIR)" ]; then rm -rf $(BUSYBOX_SRCDIR); fi
	@make --no-print-directory -i $(BUSYBOX_T)-clean
	@rm -f .$(BUSYBOX_T)-init .$(BUSYBOX_T)-unpack .$(BUSYBOX_T)-get .$(BUSYBOX_T)-patch


