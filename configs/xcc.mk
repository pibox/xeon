# ---------------------------------------------------------------
# Build the cross compiler
# We do this separate from the rest of the build to 
# avoid having to rebuild the cross compiler every time.
# Crosstool-NG is used so we can have a glibc-based system.
# (Buildroot's xcc only builds for uClibc).
#
# These targets are common to every board, but they may call
# board specific targets in xcc.mk.$(HW).
# ---------------------------------------------------------------

# Include board specific targets
include configs/xcc.mk.$(HW).$(XV)

# Retrieve Crosstools-NG package
$(XCC_T)-get: .$(XCC_T)-get

.$(XCC_T)-get: 
	@mkdir -p $(BLDDIR) $(XCC_ARCDIR)
	@if [ ! -f $(XCC_ARCDIR)/$(XCC_VERSION).tar.gz ]; then \
		$(MSG) "================================================================"; \
		$(MSG3) Retrieving XCC build package $(EMSG); \
		$(MSG) "================================================================"; \
		D=$(XCC_ARCDIR) S=$(XCC_PKG_NAME) U=$(XCC_URL) make --no-print-directory getsw-only; \
	else \
		$(MSG3) XCC build package is cached $(EMSG); \
	fi
	@touch .$(subst .,,$@)

# Prefetch archive that are problematic for Crosstool-NG to retrieve.
$(XCC_T)-prefetch: .$(XCC_T)-prefetch 

.$(XCC_T)-prefetch: .$(XCC_T)-get
	@for archive in $(xccarc); do \
		$(MSG3) "Retrieving $$archive" $(EMSG); \
		cd $(XCC_ARCDIR) && wget "$$archive"; \
	done
	@touch .$(subst .,,$@)

# Unpack Crosstools-NG package
$(XCC_T)-unpack: .$(XCC_T)-unpack 

.$(XCC_T)-unpack: .$(XCC_T)-prefetch
	@$(MSG3) "Unpacking XCC" $(EMSG)
	@mkdir -p $(XCC_BLDDIR) $(XCC_ARCDIR)
	@if [ ! -d $(XCC_SRCDIR) ]; then \
		tar -C $(BLDDIR) -$(XCC_JZ)xf $(XCC_ARCDIR)/$(XCC_PKG_NAME); \
	fi
	@if [ ! -d $(XCC_SRCDIR) ] && [ -d $(BLDDIR)/$(XCC_NAME) ]; then \
		mv $(BLDDIR)/$(XCC_NAME) $(XCC_SRCDIR); \
	fi
	@touch .$(subst .,,$@)

# Patch Crosstools-NG
$(XCC_T)-patch: .$(XCC_T)-patch 

.$(XCC_T)-patch: .$(XCC_T)-unpack
	@if [ -d $(DIR_PATCH) ]; then \
		for patchname in `ls -1 $(DIR_PATCH)/*.patch`; do \
			if [ -f "$$patchname" ]; then \
				$(MSG3) Applying $$patchname $(EMSG); \
				cd $(XCC_SRCDIR) && patch -N -p1 -r - < $$patchname; \
			fi; \
		done; \
	fi
	@touch .$(subst .,,$@)

# Build and install Crosstools-NG package
$(XCC_T)-init: .$(XCC_T)-init 

.$(XCC_T)-init: .$(XCC_T)-patch
	@$(MSG3) "Configuring Crosstool-NG build" $(EMSG)
	@cd $(XCC_SRCDIR) && ./configure --prefix=$(XCC_CTNGDIR) 
	@$(MSG3) "Running Crosstool-NG build" $(EMSG)
	@cd $(XCC_SRCDIR) && MAKELEVEL=0 make 
	@$(MSG3) "Installing Crosstool-NG" $(EMSG)
	@cd $(XCC_SRCDIR) && MAKELEVEL=0 make install
	@touch .$(subst .,,$@)

# Copy and update the configuation template for Crosstools-NG from our source tree
$(XCC_T)-preconfig: 
	@if [ -f $(XCC_CONFIG) ]; then \
		$(MSG3) "Running preconfig" $(EMSG); \
		cp $(XCC_CONFIG) $(XCC_BLDDIR)/.config; \
		make $(XCC_T)-preconfig-$(HW); \
	fi

$(XCC_T)-verify:
	@if [ ! -f $(XCC_BOARDCFG) ]; then \
		$(MSG11) "Missing $(XCC_BOARDCFG)" $(EMSG); \
		exit 1; \
	fi
	
# Build the cross compiler based on our requirements.
$(XCC_T): .$(XCC_T)

.$(XCC_T): .$(XCC_T)-init 
	@$(MSG) "================================================================"
	@$(MSG2) "Building XCC" $(EMSG)
	@$(MSG) "================================================================"
	@make $(XCC_T)-verify
	@make $(XCC_T)-preconfig
	@$(MSG3) "Running ct-ng build" $(EMSG)
	@cd $(XCC_BLDDIR) && \
		unset LD_LIBRARY_PATH && \
		PATH=$(XCC_CTNGDIR)/bin:$(PATH) CT_PREFIX=$(XCC_BLDDIR) ct-ng build 
	@$(MSG3) "Adding user write permissions to build tree " $(EMSG)
	@cd $(XCC_PREFIXDIR) && chmod -R u+w * 
	@$(MSG3) "Fixup build tree " $(EMSG)
	@make $(XCC_T)-fixup-$(HW)
	@touch .$(subst .,,$@)

# Edit the CT-NG configuration through its menuconfig utility
$(XCC_T)-menuconfig: .$(XCC_T)-init
	@make $(XCC_T)-preconfig
	cd $(XCC_BLDDIR) && PATH=$(XCC_CTNGDIR)/bin:$(PATH) ct-ng menuconfig

# Save the CT-NG configuration as a template in our source tree.
$(XCC_T)-saveconfig: 
	@if [ -d $(XCC_SRCDIR) ]; then \
		$(MSG) "================================================================"; \
		$(MSG2) "Saving XCC configuration for $(HW).$(XV)" $(EMSG); \
		$(MSG) "================================================================"; \
		make $(XCC_T)-saveconfig-$(HW); \
	fi

# Update config:  take the specified config and updated to the current config.
$(XCC_T)-updateconfig: 
	@cp $(XCC_OLD_CONFIG) $(XCC_BLDDIR)/.config
	@cd $(XCC_BLDDIR) && \
		unset LD_LIBRARY_PATH && \
		PATH=$(XCC_CTNGDIR)/bin:$(PATH) CT_PREFIX=$(XCC_BLDDIR) ct-ng updateconfig 

$(XCC_T)-files:
	@$(MSG) "================================================================"
	@$(MSG2) "XCC Build Files ($(XCC_BLDDIR))" $(EMSG)
	@$(MSG) "================================================================"
	@ls $(XCC_BLDDIR)

$(XCC_T)-pkg: .$(XCC_T) 
	@if [ ! -d $(XCC_PREFIXDIR) ]; then \
		$(MSG1) Cross toolchain build directory does not exist:$(EMSG); \
		$(MSG1) $(XCC_PREFIXDIR) $(EMSG); \
		$(MSG1) Cannot build Cross Toolchain package. $(EMSG); \
		exit 1; \
	fi
	@$(MSG) "================================================================"
	@$(MSG2) "Building Cross Toolchain Package" $(EMSG)
	@$(MSG) "================================================================"
	@make --quiet --no-print-directory .$(XCC_T)-pkg-rpm
	@make --quiet --no-print-directory .$(XCC_T)-pkg-deb
	@$(MSG2) "Packaging Cross Toolchain debug utilities for the target platform." $(EMSG)
	@make --quiet --no-print-directory .$(XCC_T)-opkg
	@make --quiet --no-print-directory .$(XCC_T)-opkg-pkg

# Build RPM package if this host supports it.
ifeq ($(DORPM),Yes)
.$(XCC_T)-pkg-rpm $(XCC_T)-pkg-rpm:
	rm -f $(PKGDIR)/*.rpm
	@mkdir -p $(PKGDIR)/tmp
	cp $(DIR_PKG)/$(PKG_XCCSPECFILE) $(PKGDIR)
	sed -i 's%\[--PKGPRJ--\]%$(PKGPRJ)%g' $(PKGDIR)/$(PKG_XCCSPECFILE)
	cd $(PKGDIR) && \
	mkdir -p $(PKG_XCCPKGNAME)-$(PKG_VERSION)/$(PKGPRJ)TC && \
	cp -r $(XCC_PREFIXDIR)/* $(PKG_XCCPKGNAME)-$(PKG_VERSION)/$(PKGPRJ)TC && \
	chmod -R u+w * && \
	$(DIR_PKG)/runrpm.pl -p $(PKG_XCCPKGNAME) -d $(XCC_PREFIXDIR) -a $(PKG_ARCH) \
		-i $(PKGPRJ)TC -v $(PKG_VERSION) -s $(PKG_XCCSPECFILE) -t $(PKGDIR)/tmp/
	mv $(PKGDIR)/tmp/*.rpm $(PKGDIR)
	rm -rf $(PKGDIR)/$(PKG_XCCSPECFILE) $(PKGDIR)/tmp
	@touch .$(subst .,,$@)
else
.$(XCC_T)-pkg-rpm $(XCC_T)-pkg-rpm:
endif

# Build DEB package if this host supports it.
ifeq ($(DODEB),Yes)
.$(XCC_T)-pkg-deb $(XCC_T)-pkg-deb:
	@$(MSG3) "Building .deb" $(EMSG)
	@$(MSG13) "Deb packaging of toolchain is not yet supported." $(EMSG)
else
.$(XCC_T)-pkg-deb $(XCC_T)-pkg-deb:
endif

# Create xcc-debug utilities opkg
.$(XCC_T)-opkg $(XCC_T)-opkg:
	@make --no-print-directory $(OPKG_T)-verify
	@mkdir -p $(XCC_BLDDIR)/opkg/xcc-debug/CONTROL
	@cp -ar $(XCC_OPT_DEBUGDIR)/* $(XCC_BLDDIR)/opkg/xcc-debug/
	@cp $(DIR_XCC)/opkg/control $(XCC_BLDDIR)/opkg/xcc-debug/CONTROL/control
	@cp $(DIR_XCC)/opkg/debian-binary $(XCC_BLDDIR)/opkg/xcc-debug/CONTROL/debian-binary
	sed -i 's%\[VERSION\]%$(XV)%g' $(XCC_BLDDIR)/opkg/xcc-debug/CONTROL/control
	@cd $(XCC_BLDDIR)/opkg/ && $(OPKG_DIR)/opkg-build -O xcc-debug
	@touch .$(subst .,,$@)

.$(XCC_T)-opkg-pkg $(XCC_T)-opkg-pkg:
	@if [ -f $(XCC_BLDDIR)/opkg/xcc-debug_$(XV)_arm.opk ]; then \
		mkdir -p $(PKGDIR)/opkg; \
		cp $(XCC_BLDDIR)/opkg/*.opk $(PKGDIR)/opkg; \
	else \
		$(MSG11) "XCC debug opkg is missing: $(XCC_BLDDIR)/opkg/xcc-debug_$(XV)_arm.opk " $(EMSG); \
	fi
	@touch .$(subst .,,$@)

$(XCC_T)-opkg-clean: 
	@rm -rf $(XCC_BLDDIR)/opkg/ 

# Clean out a cross compiler build but not the CT-NG package build.
$(XCC_T)-clean:
	@if [ -d "$(XCC_CTNGDIR)" ]; then cd $(XCC_CTNGDIR) && PATH=$(XCC_PATH) ct-ng clean; fi
	@if [ -d "$(XCC_BLDDIR)" ]; then chmod -R +w $(XCC_BLDDIR); fi
	@rm -rf $(XCC_BLDDIR) 
	@rm -f .$(XCC_T) .$(XCC_T)-pkg-rpm .$(XCC_T)-pkg-deb

# Clean out everything associated with XCC
$(XCC_T)-clobber: 
	@if [ -d "$(XCC_SRCDIR)" ]; then rm -rf $(XCC_SRCDIR); fi
	@if [ -d "$(XCC_CTNGDIR)" ]; then rm -rf $(XCC_CTNGDIR); fi
	@make --no-print-directory -i $(XCC_T)-clean
	@rm -f .$(XCC_T)-init .$(XCC_T)-patch .$(XCC_T)-unpack .$(XCC_T)-get \
		.$(XCC_T)-prefetch .$(XCC_T)-opkg .$(XCC_T)-opkg-pkg
