# ---------------------------------------------------------------
# Acquire and build (if necessary) a bootloader.
# ---------------------------------------------------------------
.$(BOOTLOADER_T)-validate $(BOOTLOADER_T)-validate:
	@touch .$(subst .,,$@)

.$(BOOTLOADER_T)-get $(BOOTLOADER_T)-get:
	@mkdir -p $(BOOTLOADER_ARCDIR)
	@if [ ! -f $(BOOTLOADER_ARCDIR)/$(BOOTLOADER_PKG_NAME) ]; then \
		$(MSG) "================================================================"; \
		$(MSG3) Retrieving bootloader package: $(BOOTLOADER_PKG_NAME) $(EMSG); \
		$(MSG) "================================================================"; \
		$(BOOTLOADER_CMD_GET); \
	else \
		$(MSG3) Bootloader package is cached $(EMSG); \
	fi
	@touch .$(subst .,,$@)

.$(BOOTLOADER_T)-unpack $(BOOTLOADER_T)-unpack: .$(BOOTLOADER_T)-get
	@$(MSG3) "Unpacking Bootloader" $(EMSG)
	@mkdir -p $(BOOTLOADER_SRCDIR)
	$(BOOTLOADER_CMD_UNPACK)
	@touch .$(subst .,,$@)

.$(BOOTLOADER_T)-patch $(BOOTLOADER_T)-patch: .$(BOOTLOADER_T)-unpack
ifeq ($(BOOTLOADER_DO_PATCH),1)
	@if [ -d $(BOOTLOADER_PATCHDIR) ]; then \
		for patchname in `find $(BOOTLOADER_PATCHDIR) -name "*.patch" | sort`; do \
			$(MSG3) Applying `basename $$patchname` $(EMSG); \
			cd $(BOOTLOADER_SRCDIR) && patch -Np1 -r - < $$patchname >/dev/null; \
		done; \
	fi
endif
	@touch .$(subst .,,$@)

# Setup for the bootloader build
.$(BOOTLOADER_T)-init $(BOOTLOADER_T)-init: .$(BOOTLOADER_T)-patch 
	@touch .$(subst .,,$@)

$(BOOTLOADER_T)-config: 
	@$(MSG3) "In $(BOOTLOADER_T)-config" $(EMSG)
ifeq ($(BOOTLOADER_DO_CONFIG),1)
	@$(MSG3) "Using config: $(BOOTLOADER_CONFIG)" $(EMSG)
	$(BOOTLOADER_CMD_CONFIG)
endif

.$(BOOTLOADER_T) $(BOOTLOADER_T): .$(BOOTLOADER_T)-init
ifeq ($(BOOTLOADER_DO_COMPILE),1)
	@$(MSG3) "Calling $(BOOTLOADER_T)-config" $(EMSG)
	@make $(BOOTLOADER_T)-config
	@$(MSG) "================================================================"
	@$(MSG2) "Building Bootloader" $(EMSG)
	@$(MSG) "================================================================"
	@mkdir -p $(BOOTLOADER_BLDDIR)/images $(BOOTLOADER_ARCDIR)
	$(BOOTLOADER_CMD_COMPILE)
endif
	@touch .$(subst .,,$@)

$(BOOTLOADER_T)-menuconfig: $(BOOTLOADER_T)-config
	@cd $(BOOTLOADER_SRCDIR) && make KCONFIG_CONFIG=$(BOOTLOADER_SRCDIR)/configs/$(BOOTLOADER_CONFIG) menuconfig

$(BOOTLOADER_T)-meld:
	@meld $(BOOTLOADER_SRCDIR)/configs/$(BOOTLOADER_CONFIG) $(DIR_BOOTLOADER)/$(BOOTLOADER_CONFIG)

$(BOOTLOADER_T)-saveconfig:
	@$(MSG) "================================================================"
	@$(MSG2) "Saving Bootloader configuration" $(EMSG)
	@$(MSG) "================================================================"
	@cp $(BOOTLOADER_SRCDIR)/configs/$(BOOTLOADER_CONFIG) $(DIR_BOOTLOADER)/$(BOOTLOADER_CONFIG) 

$(BOOTLOADER_T)-files:

$(BOOTLOADER_T)-pkg: .$(BOOTLOADER_T)
	@$(MSG) "================================================================"
	@$(MSG2) "Gathering for $(BOOTLOADER_T): $(BOOTLOADER_NAME)" $(EMSG)
	@$(MSG) "================================================================"
	$(BOOTLOADER_CMD_PKG)

$(BOOTLOADER_T)-clean:
	@if [ -d "$(BOOTLOADER_SRCDIR)" ]; then cd $(BOOTLOADER_SRCDIR) && make -i clean; fi
	@sudo rm -rf $(BOOTLOADER_BLDDIR) 
	@rm -f .$(BOOTLOADER_T) 

$(BOOTLOADER_T)-clobber: 
	@if [ -d "$(BOOTLOADER_SRCDIR)" ]; then rm -rf $(BOOTLOADER_SRCDIR); fi
	@make --no-print-directory -i $(BOOTLOADER_T)-clean
	@rm -f .$(BOOTLOADER_T)-init .$(BOOTLOADER_T)-unpack .$(BOOTLOADER_T)-get .$(BOOTLOADER_T)-patch


