#----------------------------------------------------------------------
# Config variables and targets.
# Bootloader: u-boot
#----------------------------------------------------------------------

# Only use this config if building for XEON
ifeq ($(PROJECT),xeon)

BOOTLOADER_T := bootloader
TARGETS      += $(BOOTLOADER_T)
PKG_TARGETS  += $(BOOTLOADER_T)

BOOTLOADER_DO_PATCH   := 1
BOOTLOADER_DO_CONFIG  := 1
BOOTLOADER_DO_COMPILE := 1

#---------------------------------------------------------------------
# Project components, versions, and urls
# Where we get the source for the main components 

ifeq ($(BS),)

# Release versions from Denx
BOOTLOADER_NAME       := u-boot
BLR                    = -2021.10
BOOTLOADER_VERSION    := $(BOOTLOADER_NAME)$(BLR)
BOOTLOADER_PKG_NAME   := $(BOOTLOADER_VERSION).tar.bz2
BOOTLOADER_URL        := https://ftp.denx.de/pub/u-boot
BOOTLOADER_JZ         := j

else

ifeq ($(BS),mgz)
BOOTLOADER_NAME       := u-boot
BOOTLOADER_COMMIT_ID  := 0719bf42931033c3109ecc6357e8adb567cb637b
BLR                    = -${BOOTLOADER_COMMIT_ID}
BOOTLOADER_VERSION    := $(BOOTLOADER_NAME)$(BLR)
BOOTLOADER_PKG_NAME   := $(BOOTLOADER_VERSION).tar.gz
BOOTLOADER_URL        := https://source.denx.de/u-boot/u-boot/-/archive/${BOOTLOADER_COMMIT_ID}
BOOTLOADER_JZ         := z
endif

ifeq ($(BS),manjaro)
# Git version from denx used by Manjaro
BOOTLOADER_NAME       := u-boot
BLR                    = -manjaro
BOOTLOADER_GIT_COMMIT := 0719bf42931033c3109ecc6357e8adb567cb637b
BOOTLOADER_VERSION    := $(BOOTLOADER_NAME)$(BLR)
BOOTLOADER_PKG_NAME   := $(BOOTLOADER_NAME)$(BLR)
BOOTLOADER_URL        := https://source.denx.de/u-boot/u-boot.git
BOOTLOADER_JZ         := 
endif

endif

# u-boot might not have the same arch type as the kernel.
BOOTLOADER_ARCH       := arm

#---------------------------------------------------------------------
# Directories: 
# The src, build and archive downloads are kept in separate
DIR_BOOTLOADER      := $(SRCDIR)/bootloader

# Root filesystem related directories
BOOTLOADER_SRCDIR   := $(BLDDIR)/$(BOOTLOADER_VERSION)
BOOTLOADER_BLDDIR   := $(BLDDIR)/$(BOOTLOADER_VERSION).bld
BOOTLOADER_ARCDIR   := $(ARCDIR)/$(BOOTLOADER_NAME)
BOOTLOADER_PKGDIR   := $(PKGDIR)/$(BOOTLOADER_NAME)
BOOTLOADER_PATCHDIR := $(DIR_BOOTLOADER)/patches/$(BOOTLOADER_NAME)$(BLR)

#---------------------------------------------------------------------
# Configuration Files
BOOTLOADER_CONFIG   := pinephone-pro-rk3399_defconfig

#---------------------------------------------------------------------
# Build commands specific to this component

ifeq ($(BS),)
# Release versions from Denx
BOOTLOADER_CMD_GET    :=  \
    D=$(BOOTLOADER_ARCDIR) S=$(BOOTLOADER_PKG_NAME) U=$(BOOTLOADER_URL) make --no-print-directory getsw-only

BOOTLOADER_CMD_UNPACK := tar -C $(BOOTLOADER_SRCDIR) --strip-components=1 -xf $(BOOTLOADER_ARCDIR)/$(BOOTLOADER_PKG_NAME) 

else

ifeq ($(BS),mgz)
BOOTLOADER_CMD_GET    :=  \
    D=$(BOOTLOADER_ARCDIR) S=$(BOOTLOADER_PKG_NAME) U=$(BOOTLOADER_URL) make --no-print-directory getsw-only

BOOTLOADER_CMD_UNPACK := tar -C $(BOOTLOADER_SRCDIR) --strip-components=1 -xf $(BOOTLOADER_ARCDIR)/$(BOOTLOADER_PKG_NAME) 
endif

ifeq ($(BS),manjaro)
# Git version from denx used by Manjaro
BOOTLOADER_CMD_GET    :=  \
	if [ ! -d $(BOOTLOADER_ARCDIR)/$(BOOTLOADER_PKG_NAME) ]; then \
    	cd $(BOOTLOADER_ARCDIR) && \
    	git clone $(BOOTLOADER_URL) $(BOOTLOADER_PKG_NAME); \
	fi; \
    cd $(BOOTLOADER_ARCDIR)/$(BOOTLOADER_PKG_NAME) && git checkout $(BOOTLOADER_GIT_COMMIT)

BOOTLOADER_CMD_UNPACK := rsync -a $(BOOTLOADER_ARCDIR)/$(BOOTLOADER_PKG_NAME) $(BLDDIR)/
endif

endif

BOOTLOADER_CMD_CONFIG := \
	mkdir -p $(BOOTLOADER_BLDDIR) && \
	cp $(DIR_BOOTLOADER)/$(BOOTLOADER_CONFIG) $(BOOTLOADER_SRCDIR)/configs/$(BOOTLOADER_CONFIG) && \
	cd $(BOOTLOADER_SRCDIR) && make O=$(BOOTLOADER_BLDDIR) $(BOOTLOADER_CONFIG)

BOOTLOADER_CMD_COMPILE:= \
	cd $(BOOTLOADER_SRCDIR) && \
	PATH=$(BOOTLOADER_PATH) make -j$(JOBS) \
		O=$(BOOTLOADER_BLDDIR) ARCH=$(BOOTLOADER_ARCH) CROSS_COMPILE=$(CROSS_COMPILER) EXTRAVERSION=-$(PROJECT)

BOOTLOADER_CMD_PKG    := \
	mkdir -p "${BOOTLOADER_PKGDIR}" && \
	cd "$(BOOTLOADER_BLDDIR)" && cp idbloader.img u-boot.itb "${BOOTLOADER_PKGDIR}"

#---------------------------------------------------------------------
# Config display target
$(BOOTLOADER_T)-showconfig:
	@make --no-print-directory showconfig
	@$(MSG3) "Bootloader Configuration" $(EMSG)
	@echo "BOOTLOADER_NAME    : $(BOOTLOADER_NAME)"
	@echo "BOOTLOADER_VERSION : $(BOOTLOADER_VERSION)"
	@echo "BOOTLOADER_PKG_NAME: $(BOOTLOADER_PKG_NAME)"
	@echo "BOOTLOADER_URL     : $(BOOTLOADER_URL)"
	@echo "BOOTLOADER_JZ      : $(BOOTLOADER_JZ)"
	@echo "DIR_BOOTLOADER     : $(DIR_BOOTLOADER)"
	@echo "BOOTLOADER_PATH    : $(BOOTLOADER_PATH)"
	@echo "BOOTLOADER_SRCDIR  : $(BOOTLOADER_SRCDIR)"
	@echo "BOOTLOADER_BLDDIR  : $(BOOTLOADER_BLDDIR)"
	@echo "BOOTLOADER_ARCDIR  : $(BOOTLOADER_ARCDIR)"
	@echo "BOOTLOADER_CONFIG  : $(BOOTLOADER_CONFIG)"
	@echo "BOOTLOADER_PATCHDIR: $(BOOTLOADER_PATCHDIR)"

endif
