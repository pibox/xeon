# ---------------------------------------------------------------
# Build the root filesystem via Buildroot
# ---------------------------------------------------------------
# Verify the existance of required components, if any
$(BUILDROOT_T)-verify:
	@if [ "x$(XCC_PREFIXDIR)" = "x" ]; then \
		$(MSG11) "Missing XCC_PREFIXDIR" $(EMSG); \
		$(MSG11) "Try setting XI on the command line." $(EMSG); \
		exit 1; \
	fi
	@if [ ! -d $(XCC_PREFIXDIR) ]; then \
		$(MSG11) "No such directory: $(XCC_PREFIXDIR)" $(EMSG); \
		$(MSG11) "Try setting XI on the command line." $(EMSG); \
		exit 1; \
	fi

.$(BUILDROOT_T)-validate $(BUILDROOT_T)-validate:
	@touch .$(subst .,,$@)

.$(BUILDROOT_T)-get $(BUILDROOT_T)-get: .$(BUILDROOT_T)-validate
	@mkdir -p $(BLDDIR) $(BUILDROOT_ARCDIR)
	@if [ ! -f $(BUILDROOT_ARCDIR)/$(BUILDROOT_VERSION).tar.bz2 ]; then \
		$(MSG) "================================================================"; \
		$(MSG3) Retrieving Buildroot package $(EMSG); \
		$(MSG) "================================================================"; \
		D=$(BUILDROOT_ARCDIR) S=$(BUILDROOT_PKG_NAME) U=$(BUILDROOT_URL) $(MAKE) --no-print-directory getsw-only; \
	else \
		$(MSG3) Buildroot $(BUILDROOT_RELEASE) package is cached $(EMSG); \
	fi
	@touch .$(subst .,,$@)

# Unpack the archive into the build directory.  
# If the archive was a snapshot, we need to rename the unpacked directory.
.$(BUILDROOT_T)-unpack $(BUILDROOT_T)-unpack: .$(BUILDROOT_T)-get
	@$(MSG3) "Unpacking Buildroot" $(EMSG)
	@mkdir -p $(BUILDROOT_BLDDIR) $(BUILDROOT_ARCDIR)
	@if [ ! -d $(BUILDROOT_SRCDIR) ]; then \
		tar -C $(BLDDIR) -$(BUILDROOT_JZ)xf $(BUILDROOT_ARCDIR)/$(BUILDROOT_PKG_NAME); \
	fi
	@if [ -d $(BLDDIR)/buildroot ]; then \
		mv $(BLDDIR)/buildroot $(BLDDIR)/$(BUILDROOT_VERSION); \
	fi
	@touch .$(subst .,,$@)

.$(BUILDROOT_T)-patch $(BUILDROOT_T)-patch: .$(BUILDROOT_T)-unpack
	@if [ -d $(DIR_BUILDROOT)/patches/$(BUILDROOT_RELEASE) ]; then \
		for patchname in `ls -1 $(DIR_BUILDROOT)/patches/$(BUILDROOT_RELEASE)/*.patch`; do \
			$(MSG3) Applying $$patchname $(EMSG); \
			cd $(BUILDROOT_SRCDIR) && patch -Np1 -r - < $$patchname; \
		done; \
	fi
	@touch .$(subst .,,$@)

.$(BUILDROOT_T)-package $(BUILDROOT_T)-package: .$(BUILDROOT_T)-patch
	@if [ -d $(DIR_BUILDROOT)/package/$(BUILDROOT_RELEASE) ]; then \
		for pkgname in `ls -1 $(DIR_BUILDROOT)/package/$(BUILDROOT_RELEASE)`; do \
			if [ "$$pkgname" != "CVS" ]; then \
				$(MSG3) Installing $$pkgname $(EMSG); \
				rsync -ar --exclude "CVS/" \
					$(DIR_BUILDROOT)/package/$(BUILDROOT_RELEASE)/$$pkgname $(BUILDROOT_SRCDIR)/package/; \
			fi; \
		done; \
	fi
	@touch .$(subst .,,$@)

# Setup for the root filesystem build
.$(BUILDROOT_T)-init $(BUILDROOT_T)-init: .$(BUILDROOT_T)-package 
	@touch .$(subst .,,$@)

# Preconfiguration happens each time the build is run, right before the compilation
# starts.  Doing it each time allows us to apply our more recent changes to the build tree.
# The process includes:
# 1. Install our buildroot packages
# 2. Install our target skeleton
# 3. Grab kernel modules, firmware and other customizations
$(BUILDROOT_T)-preconfig:
	@$(MSG3) "Preconfig" $(EMSG)
	@mkdir -p $(BUILDROOT_SRCDIR)/package/customize/source
	@$(MAKE) --no-print-directory $(BUILDROOT_T)-preconfig-customize
	@if [ -d $(KERNEL_SRCDIR)/modules ]; then \
		$(MSG3) "Grabbing kernel modules" $(EMSG); \
		rsync -ar $(KERNEL_SRCDIR)/modules/* $(BUILDROOT_SRCDIR)/package/customize/source; \
		$(MSG3) "Fixing symlinks" $(EMSG); \
		cd $(BUILDROOT_SRCDIR)/package/customize/source/lib/modules/ && ls -1 > $(BUILDROOT_SRCDIR)/kernel.version; \
		cd $(BUILDROOT_SRCDIR)/package/customize/source/lib/modules/`cat $(BUILDROOT_SRCDIR)/kernel.version`/ && rm -f build source ; \
		cd $(BUILDROOT_SRCDIR)/package/customize/source/lib/modules/`cat $(BUILDROOT_SRCDIR)/kernel.version`/ && ln -s /usr/src/kernels/$(KV)+ build ; \
		cd $(BUILDROOT_SRCDIR)/package/customize/source/lib/modules/`cat $(BUILDROOT_SRCDIR)/kernel.version`/ && ln -s build source ; \
		rm $(BUILDROOT_SRCDIR)/kernel.version; \
	else \
		$(MSG11) "Can't find kernel modules" $(EMSG); \
	fi
	@$(MAKE) --no-print-directory $(BUILDROOT_T)-$(PROJECT)-preconfig

$(BUILDROOT_T)-preconfig-customize:
	@$(MSG3) "Preconfig Customize" $(EMSG)
	@rm -f $(BUILDROOT_SRCDIR)/output/build/.customize
	@rm -rf $(BUILDROOT_SRCDIR)/package/customize/source/*
	@rm -f $(BUILDROOT_SRCDIR)/package/customize/source/.empty
	@rsync -ar --exclude "CVS/" $(BUILDROOT_SKELETON)/* $(BUILDROOT_SRCDIR)/package/customize/source
	@chmod 755 $(BUILDROOT_SRCDIR)/package/customize/source/etc/init.d/*
	@echo "PiBox $(BLD_VERSION)" > $(BUILDROOT_SRCDIR)/package/customize/source/etc/pibox-version
	@echo "HW $(HW)" >> $(BUILDROOT_SRCDIR)/package/customize/source/etc/pibox-version
	@echo "$(BLD_DATE)" >> $(BUILDROOT_SRCDIR)/package/customize/source/etc/pibox-version
	@sed -i 's%\[VERSION\]%$(BLD_VERSION)%g' $(BUILDROOT_SRCDIR)/package/customize/source/etc/os-release
	@sed -i 's%\[HW\]%$(HW)%g' $(BUILDROOT_SRCDIR)/package/customize/source/etc/os-release
	@sed -i 's%\[PLATFORM\]%$(PLATFORM)%g' $(BUILDROOT_SRCDIR)/package/customize/source/etc/os-release
	@sed -i 's%\[BUILDID\]%$(BLD_DATE)%g' $(BUILDROOT_SRCDIR)/package/customize/source/etc/os-release
	@rsync -ar $(XCC_PREFIXDIR)/$(XCC_PREFIX)/sysroot/* $(BUILDROOT_SRCDIR)/package/customize/source
	@$(MAKE) --no-print-directory $(BUILDROOT_T)-$(PROJECT)-preconfig-customize

# Config target copies in our copies of the configuration files for buildroot
# and busybox, updating tags with real values.  This gets run with each
# build so the latest configuration is picked up.  Use *-menuconfig and 
# *-saveconfig to edit and save configuration changes.
$(BUILDROOT_T)-config:
	@$(MSG3) "Configuring Buildroot" $(EMSG)
	@cp $(BUILDROOT_CONFIG) $(BUILDROOT_SRCDIR)/.config
	@cp $(BUILDROOT_PBLD_SRC) $(BUILDROOT_POSTBLD_SCRIPT)
	@sed -i 's%\[CUSTOMSRC\]%$(BUILDROOT_SRCDIR)/package/customize/source%g' $(BUILDROOT_POSTBLD_SCRIPT)
	@sed -i 's%\[LEGALSRC\]%$(BUILDROOT_SRCDIR)/output/legal-info%g' $(BUILDROOT_POSTBLD_SCRIPT)
	@sed -i 's%\[DLDIR\]%$(BUILDROOT_ARCDIR)%g' $(BUILDROOT_SRCDIR)/.config
	@sed -i 's%\[BLDDIR\]%$(BUILDROOT_BLDDIR)%g' $(BUILDROOT_SRCDIR)/.config
	@sed -i 's%\[XCC_PREFIXDIR\]%$(XCC_PREFIXDIR)%g' $(BUILDROOT_SRCDIR)/.config
	@sed -i 's%\[CTNG_SAMPLE\]%$(CTNG_SAMPLE)%g' $(BUILDROOT_SRCDIR)/.config
	@sed -i 's%\[POSTBLD\]%$(BUILDROOT_POSTBLD_SCRIPT)%g' $(BUILDROOT_SRCDIR)/.config
	@sed -i 's%BR2_JLEVEL=.*%BR2_JLEVEL=$(JOBS)%g' $(BUILDROOT_SRCDIR)/.config
	@cp $(BUILDROOT_BUSYBOX_CONFIG) $(BUILDROOT_SRCDIR)/.busybox_config
	@sed -i 's%\[BLDDIR\]%$(BUILDROOT_BLDDIR)%g' $(BUILDROOT_SRCDIR)/.busybox_config
	@mkdir -p $(BUILDROOT_BLDDIR)/staging/host
	@cp $(DIR_BUILDROOT)/toolchainfile.cmake $(BUILDROOT_BLDDIR)/staging/host
	@sed -i 's%\[XCC\]%$(XCC_PREFIXDIR)/bin/$(XCC_PREFIX)-gcc%g' $(BUILDROOT_BLDDIR)/staging/host/toolchainfile.cmake
	@sed -i 's%\[XCC++\]%$(XCC_PREFIXDIR)/bin/$(XCC_PREFIX)-g++%g' $(BUILDROOT_BLDDIR)/staging/host/toolchainfile.cmake
	@sed -i 's%\[STAGING\]%$(BUILDROOT_BLDDIR)/staging%g' $(BUILDROOT_BLDDIR)/staging/host/toolchainfile.cmake
	@sed -i 's%\[SYSROOT\]%$(BUILDROOT_SYSROOT)%g' $(BUILDROOT_BLDDIR)/staging/host/toolchainfile.cmake
	@sed -i 's%\[TARGETFS\]%$(BUILDROOT_TARGETFS)%g' $(BUILDROOT_BLDDIR)/staging/host/toolchainfile.cmake

# This makes it possible to rebuild the rootfs by first cleaning out the custom package
# and then rerunning buildroot, which will just rebuild its components with the updated
# custom package.
$(BUILDROOT_T)-rebuild: .$(BUILDROOT_T)-clean-custom
	@$(MSG3) "Forcing rebuild of root filesystem" $(EMSG)
	@rm -f $(BUILDROOT_SRCDIR)/output/build/.root .$(BUILDROOT_T)
	@$(MAKE) --no-print-directory $(BUILDROOT_T)

.$(BUILDROOT_T)-clean-custom:
	@$(MSG3) "Cleaning custom files from filesystem" $(EMSG)
	@cd $(BUILDROOT_SRCDIR)/package/customize/source/ && \
		for file in `find . -type f -o -type l`; do \
			if [ -e $$file -a -f $$file ]; then \
				rm -f $(BUILDROOT_SRCDIR)/output/target/$$file; \
			fi; \
		done
	@rm -rf $(BUILDROOT_SRCDIR)/package/customize/source/* 

$(BUILDROOT_T): .$(BUILDROOT_T)

.$(BUILDROOT_T): .$(BUILDROOT_T)-init
	@$(MAKE) -s --no-print-directory $(buildroot)-verify 
	@$(MSG) "================================================================"
	@$(MSG2) "Building $(BUILDROOT_T)" $(EMSG)
	@$(MSG) "================================================================"
	@mkdir -p $(BUILDROOT_BLDDIR)/images $(BUILDROOT_ARCDIR)
	@$(MAKE) --no-print-directory $(BUILDROOT_T)-preconfig
	@$(MAKE) --no-print-directory $(BUILDROOT_T)-config
	@cd $(BUILDROOT_SRCDIR) && $(MAKE) oldconfig
	@$(MAKE) --no-print-directory $(BUILDROOT_T)-legal-info
	@$(MSG3) "Running BUILDROOT build" $(EMSG)
	@cd $(BUILDROOT_SRCDIR) && $(MAKE) -j $(JOBS) LD_LIBRARY_PATH= BUSYBOX_CONFIG=$(BUILDROOT_SRCDIR)/.busybox_config
	@$(MAKE) --no-print-directory $(BUILDROOT_T)-$(PROJECT)
	@ls -l $(BUILDROOT_SRCDIR)/output/images/
	@touch .$(subst .,,$@)

$(BUILDROOT_T)-menuconfig:
	@$(MAKE) --no-print-directory $(BUILDROOT_T)-config
	@cd $(BUILDROOT_SRCDIR) && $(MAKE) BUSYBOX_CONFIG=$(BUILDROOT_SRCDIR)/.busybox_config menuconfig

$(BUILDROOT_T)-oldconfig:
	@cd $(BUILDROOT_SRCDIR) && $(MAKE) BUSYBOX_CONFIG=$(BUILDROOT_SRCDIR)/.busybox_config oldconfig

$(BUILDROOT_T)-meld:
	@meld $(BUILDROOT_SRCDIR)/.config $(BUILDROOT_CONFIG)

$(BUILDROOT_T)-diff:
	@diff -u $(BUILDROOT_SRCDIR)/.config $(BUILDROOT_CONFIG)

# Build an ext3 filesystem from the buildroot package
$(BUILDROOT_T)-ext3:
	@$(MSG3) "Generating EXT3 filesystem image" $(EMSG)
	@mkdir -p $(BUILDROOT_BLDDIR)/mnt
	@dd status=none if=/dev/zero of=$(BUILDROOT_SRCDIR)/output/images/rootfs.ext3 bs=1024 count=2000000
	@mke2fs -q -F -j $(BUILDROOT_SRCDIR)/output/images/rootfs.ext3
	@sudo mount -o loop $(BUILDROOT_SRCDIR)/output/images/rootfs.ext3 $(BUILDROOT_BLDDIR)/mnt
	@$(MSG3) "Unpacking rootfs to EXT3 image" $(EMSG)
	@sudo tar -C $(BUILDROOT_BLDDIR)/mnt -xf $(BUILDROOT_SRCDIR)/output/images/rootfs.tar
	@$(MAKE) --no-print-directory $(BUILDROOT_T)-$(PROJECT)-ext3
	@sudo umount $(BUILDROOT_BLDDIR)/mnt

$(BUILDROOT_T)-saveconfig:
	@$(MSG) "================================================================"
	@$(MSG2) "Saving BUILDROOT configuration" $(EMSG)
	@$(MSG) "================================================================"
	@cp $(BUILDROOT_SRCDIR)/.config $(BUILDROOT_CONFIG) 
	@sed -i 's%$(BUILDROOT_ARCDIR)%\[DLDIR\]%g' $(BUILDROOT_CONFIG)
	@sed -i 's%$(BUILDROOT_BLDDIR)%\[BLDDIR\]%g' $(BUILDROOT_CONFIG)
	@sed -i 's%$(XCC_PREFIXDIR)%\[XCC_PREFIXDIR\]%g' $(BUILDROOT_CONFIG)
	@sed -i 's%$(CTNG_SAMPLE)%\[CTNG_SAMPLE\]%g' $(BUILDROOT_CONFIG)
	@sed -i 's%$(BUILDROOT_POSTBLD_SCRIPT)%\[POSTBLD\]%g' $(BUILDROOT_CONFIG)
	@sed -i 's%BR2_JLEVEL=.*%BR2_JLEVEL=8%g' $(BUILDROOT_CONFIG)

$(BUILDROOT_T)-files: .$(BUILDROOT_T)
	@$(MSG) "================================================================"
	@$(MSG2) "$(BUILDROOT_T) artifacts" $(EMSG)
	@$(MSG) "================================================================"
	@ls -l $(BUILDROOT_SRCDIR)/output/images/

$(BUILDROOT_T)-legal-info:
	@$(MSG3) "Generating Legal Info" $(EMSG)
	@sudo rm -rf $(BUILDROOT_SRCDIR)/output/legal-info
	@cd $(BUILDROOT_SRCDIR) && $(MAKE) -j $(JOBS) LD_LIBRARY_PATH= legal-info

$(BUILDROOT_T)-pkg: .$(BUILDROOT_T) 
	@$(MSG) "================================================================"
	@$(MSG2) "Gathering for $(BUILDROOT_T)" $(EMSG)
	@$(MSG) "================================================================"
	@$(MAKE) --no-print-directory -i $(BUILDROOT_T)-ext3
	@$(MSG3) "Grabbing $(BUILDROOT_T) ext3 image" $(EMSG)
	@if [ -f $(BUILDROOT_SRCDIR)/output/images/rootfs.ext3 ]; then \
		cp $(BUILDROOT_SRCDIR)/output/images/rootfs.ext3 $(PKGDIR)/rootfs.ext3; \
	else \
		$(MSG11) "Missing ext3 rootfs image: $(BUILDROOT_SRCDIR)/output/images/rootfs.ext3" $(EMSG); \
	fi
	@if [ -f $(BUILDROOT_SRCDIR)/output/images/rootfs.squashfs ]; then \
		cp $(BUILDROOT_SRCDIR)/output/images/rootfs.squashfs $(PKGDIR)/rootfs.squashfs; \
	else \
		$(MSG11) "Missing squashfs rootfs image: $(BUILDROOT_SRCDIR)/output/images/rootfs.squashfs" $(EMSG); \
	fi
	@$(MSG3) "Building $(BUILDROOT_T) staging archive for use with cross compiles" $(EMSG)
	@cd $(BUILDROOT_SRCDIR)/output/staging/ && tar --transform="s%^\./%staging/%" -czf $(PKGDIR)/staging.tar.gz .

$(BUILDROOT_T)-pkg-clean: 
	@rm -f $(PKGDIR)/staging.tar.gz $(PKGDIR)/ramdisk.tar

$(BUILDROOT_T)-clean:
	@if [ -d "$(BUILDROOT_SRCDIR)" ]; then cd $(BUILDROOT_SRCDIR) && $(MAKE) -i clean; fi
	@sudo rm -rf $(BUILDROOT_BLDDIR) 
	@rm -f .$(BUILDROOT_T) 

$(BUILDROOT_T)-clobber: 
	@if [ -d "$(BUILDROOT_SRCDIR)" ]; then rm -rf $(BUILDROOT_SRCDIR); fi
	@$(MAKE) --no-print-directory -i $(BUILDROOT_T)-clean
	@rm -f .$(BUILDROOT_T)-init .$(BUILDROOT_T)-package .$(BUILDROOT_T)-patch .$(BUILDROOT_T)-unpack \
		.$(BUILDROOT_T)-get .$(BUILDROOT_T)-validate 


