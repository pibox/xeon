# ---------------------------------------------------------------
# Xeon specific build targets.
# ---------------------------------------------------------------
#
# ---------------------------------------------------------------
# REQUIRED targets: buildroot.mk will call these
# ---------------------------------------------------------------
$(BUILDROOT_T)-xeon-preconfig:

$(BUILDROOT_T)-xeon-preconfig-customize:
	@if [ $(XEON) -eq 1 ]; then \
		if [ ! -f $(DIR_BUILDROOT)/xeon/pointercal.landscape ]; then \
			$(MSG11) "Missing $(DIR_BUILDROOT)/xeon/pointercal.landscape" $(EMSG) ; \
		fi; \
		if [ ! -d $(BUILDROOT_SRCDIR)/package/customize/source/etc ]; then \
			$(MSG11) "Missing package/customize/source/etc " $(EMSG) ; \
		fi; \
		cp $(DIR_BUILDROOT)/xeon/pointercal.landscape \
			$(BUILDROOT_SRCDIR)/package/customize/source/etc/pointercal ; \
	fi

$(BUILDROOT_T)-xeon:

$(BUILDROOT_T)-xeon-ext3:

# ---------------------------------------------------------------
# Platform specific targets call from required targets.
# ---------------------------------------------------------------
