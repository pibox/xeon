# ---------------------------------------------------------------
# Build the Linux Kernel
# ---------------------------------------------------------------

# Verify the existance of required components, if any
$(KERNEL_T)-verify:
	@if [ "x$(XCC_PREFIXDIR)" = "x" ]; then \
		$(MSG11) "Missing XCC_PREFIXDIR" $(EMSG); \
		$(MSG11) "Try setting XI on the command line." $(EMSG); \
		exit 1; \
	fi
	@if [ ! -d $(XCC_PREFIXDIR) ]; then \
		$(MSG11) "No such directory: $(XCC_PREFIXDIR)" $(EMSG); \
		$(MSG11) "Try setting XI on the command line." $(EMSG); \
		exit 1; \
	fi

# Retrieve kernel patches, if any
# Patches must be archived in a single file, retrieved via an HTTP url.
.$(KERNEL_T)-get-patches $(KERNEL_T)-get-patches:
	@if [ "x$(KERNEL_PATCH_URL)" != "x" ]; then \
		if [ ! -f $(KERNEL_ARCDIR)/$(KERNEL_PATCH) ]; then \
			$(MSG) "================================================================"; \
			$(MSG3) Retrieving Kernel Patches package $(EMSG); \
			$(MSG) "================================================================"; \
			D=$(KERNEL_ARCDIR) S=$(KERNEL_PATCH) U=$(KERNEL_PATCH_URL) make --no-print-directory getsw-only; \
		else \
			$(MSG3) Kernel Patches package is cached $(EMSG); \
		fi; \
	fi
	@touch .$(subst .,,$@)

# Retrieve the kernel.
# GIT downloaded check for $(KERNEL_ARCDIR)/$(KERNEL_VERSION)
# HTTP downloaded check for $(KERNEL_ARCDIR)/$(KERNEL_PKG), which is KERNEL_VERSION + tar.<gz,bz2>
.$(KERNEL_T)-get $(KERNEL_T)-get: .$(KERNEL_T)-get-patches
	@mkdir -p $(BLDDIR) $(KERNEL_ARCDIR) 
	@if [ "$(KERNEL_PROTOCOL)" = "git" ]; then \
		if [ ! -d $(KERNEL_ARCDIR)/$(KSRC) ]; then \
			$(MSG) "================================================================"; \
			$(MSG3) "Retrieving Kernel source into $(KERNEL_ARCDIR)/$(KSRC)" $(EMSG); \
			$(MSG) "================================================================"; \
			cd $(KERNEL_ARCDIR) && git clone $(KERNEL_URL) $(KSRC); \
		else \
			$(MSG3) "Kernel source is cached in $(KERNEL_ARCDIR)/$(KSRC) - doing pull" $(EMSG); \
		    cd $(KERNEL_ARCDIR)/$(KSRC) && git pull; \
		fi; \
		$(MSG3) "Checking out branch: $(KERNEL_GIT_ID)" $(EMSG); \
		cd $(KERNEL_ARCDIR)/$(KSRC) && git checkout $(KERNEL_GIT_ID); \
	else \
		if [ ! -f $(KERNEL_ARCDIR)/$(KERNEL_PKG) ]; then \
			$(MSG) "================================================================"; \
			$(MSG3) "Retrieving Kernel source" $(EMSG); \
			$(MSG) "================================================================"; \
			cd $(KERNEL_ARCDIR) && wget $(KERNEL_URL)/$(KERNEL_PKG); \
		else \
			$(MSG3) Kernel source is cached $(EMSG); \
		fi; \
	fi
	@touch .$(subst .,,$@)

# Unpack the kernel.  
# GIT gets copied from ARCDIR. 
# HTTP gets unpacked from its archive file in ARCDIR.
.$(KERNEL_T)-unpack $(KERNEL_T)-unpack: .$(KERNEL_T)-get
	@if [ ! -d $(KERNEL_SRCDIR) ]; then \
		if [ "$(KERNEL_PROTOCOL)" = "git" ]; then \
			if [ -d $(KERNEL_ARCDIR)/$(KSRC) ]; then \
				$(MSG3) "Copying Linux Kernel archive to build dir" $(EMSG); \
				mkdir -p $(KERNEL_SRCDIR); \
				rsync -a $(KERNEL_ARCDIR)/$(KSRC)/ $(KERNEL_SRCDIR)/; \
			else \
				$(MSG11) "Kernel source archive is missing:" $(EMSG); \
				$(MSG11) "$(KERNEL_ARCDIR)/$(KSRC) not found " $(EMSG); \
				exit 1; \
			fi; \
		else \
			if [ -f $(KERNEL_ARCDIR)/$(KERNEL_PKG) ]; then \
				$(MSG3) "Unpacking Linux Kernel archive to build dir" $(EMSG); \
				cd $(KERNEL_ARCDIR) && tar -C $(BLDDIR) -x$(KERNEL_JZ)f $(KERNEL_PKG); \
			else \
				$(MSG11) "Kernel source archive is missing:" $(EMSG); \
				$(MSG11) "$(KERNEL_ARCDIR)/$(KERNEL_PKG) not found " $(EMSG); \
				exit 1; \
			fi; \
		fi; \
	fi
	@touch .$(subst .,,$@)

.$(KERNEL_T)-get-drivers $(KERNEL_T)-get-drivers: .$(KERNEL_T)-unpack
	@for drvr in $(drsrc); do \
		$(MSG3) "Retrieving $$drvr" $(EMSG); \
		make --no-print-directory $$drvr-get; \
	done
	@touch .$(subst .,,$@)

# Unpack patches (if any) into kernel source tree.
# We may have patches from three sources:
# 1. Downloaded from a URL (stored in ARCDIR)
# 2. Available locally (specified by KERNEL_PATCH_DIR)
# KERNEL_SRCDIR/patches is where the patches are unpacked.
.$(KERNEL_T)-unpack-patches $(KERNEL_T)-unpack-patches: .$(KERNEL_T)-get-drivers
	@if [ "x$(KERNEL_PATCH_URL)" != "x" ]; then \
		$(MSG3) "Unpacking downloaded kernel patches." $(EMSG); \
		mkdir -p $(KERNEL_SRCDIR)/patches; \
		tar -C $(KERNEL_SRCDIR)/patches -$(KERNEL_PATCH_JZ)xf $(KERNEL_ARCDIR)/$(KERNEL_PATCH); \
		cp $(KERNEL_ARCDIR)/*.patch $(KERNEL_SRCDIR)/patches; \
		cp $(DIR_KERNEL)/patches/*.patch $(KERNEL_SRCDIR)/patches; \
	else \
		$(MSG3) "No downloaded kernel patches to unpack." $(EMSG); \
	fi
	@if [ "x$(KERNEL_PATCH_DIR)" != "x" ] && \
		[ "$(KERNEL_PATCH_DIR)" != "$(KERNEL_SRCDIR)/patches" ] && \
		[ -d "$(KERNEL_PATCH_DIR)" ]; then \
		$(MSG3) "Unpacking kernel patches from KERNEL_PATCH_DIR." $(EMSG); \
		mkdir -p $(KERNEL_SRCDIR)/patches; \
		rsync -ar --exclude "CVS/" $(KERNEL_PATCH_DIR)/* $(KERNEL_SRCDIR)/patches; \
	else \
		$(MSG3) "No patches from KERNEL_PATCH_DIR." $(EMSG); \
	fi
	@touch .$(subst .,,$@)

# Patch the kernel
.$(KERNEL_T)-patch $(KERNEL_T)-patch: .$(KERNEL_T)-unpack-patches
	@if [ -d $(KERNEL_SRCDIR)/patches ]; then \
		$(MSG3) "Patching Linux Kernel" $(EMSG); \
		cd $(KERNEL_SRCDIR)/patches && find . -name "*.patch" | sort > $(KERNEL_SRCDIR)/.series; \
		if [ -s $(KERNEL_SRCDIR)/.series ]; then \
			mv $(KERNEL_SRCDIR)/.series $(KERNEL_SRCDIR)/patches/series; \
			cd $(KERNEL_SRCDIR) && quilt push -a; \
		else \
			rm $(KERNEL_SRCDIR)/.series; \
		fi; \
	else \
		$(MSG3) "There are no kernel patches to apply." $(EMSG); \
	fi
	@touch .$(subst .,,$@)

# Get, unpack and patch kernel, as needed
.$(KERNEL_T)-init $(KERNEL_T)-init: .$(KERNEL_T)-patch
	@make -s --no-print-directory $(KERNEL_T)-verify 
	@touch .$(subst .,,$@)

# Copy configuation template from our source tree to unpacked source tree
# Prep kernel source tree based on appropriate configuration.
$(KERNEL_T)-preconfig: 
	@if [ -f $(KERNEL_CONFIG) ]; then \
		$(MSG3) "Using local config" $(EMSG); \
		cp $(KERNEL_CONFIG) $(KERNEL_SRCDIR)/.config; \
		cd $(KERNEL_SRCDIR) && PATH=$(KERNEL_PATH) \
			make -j$(JOBS) ARCH=$(ARCH) CROSS_COMPILE=$(CROSS_COMPILER) oldconfig; \
	else \
		$(MSG6) "No local config - using $(KERNEL_DEFCONFIG)" $(EMSG); \
		cd $(KERNEL_SRCDIR) && PATH=$(KERNEL_PATH) \
			make -j$(JOBS) ARCH=$(ARCH) CROSS_COMPILE=$(CROSS_COMPILER) $(KERNEL_DEFCONFIG); \
	fi

# Grab firmware files, if any
# 1. Retrieve release files via the fw.cfg file.
# 2. Copy in local firmware - stuff without release archives that are easy to download.
$(KERNEL_T)-firmware-get: 
	@if [ ! -d $(KERNEL_ARCDIR)/$(KERNEL_FW_VERSION) ]; then \
		$(MSG) "================================================================"; \
		$(MSG3) Retrieving Kernel firmware repo $(EMSG); \
		$(MSG) "================================================================"; \
		cd $(KERNEL_ARCDIR) && $(KERNEL_FW_GET_CMD) ; \
	else \
		$(MSG3) Kernel firmware repo is cached: updating $(EMSG); \
		echo "Archive: $(KERNEL_ARCDIR)/$(KERNEL_FW_VERSION)" ; \
		cd $(KERNEL_ARCDIR)/$(KERNEL_FW_VERSION) && $(KERNEL_FW_UPD_CMD) ; \
	fi
	@cd $(KERNEL_ARCDIR)/$(KERNEL_FW_VERSION) && $(KERNEL_FW_CO_CMD)

$(KERNEL_T)-firmware: $(KERNEL_T)-firmware-get
	@mkdir -p $(KERNEL_SRCDIR)/modules/lib/firmware
	@if [ -d $(KERNEL_ARCDIR)/$(KERNEL_FW_VERSION) ]; then \
		$(MSG3) "Copying Linux firmware archive to build dir" $(EMSG); \
		$(KERNEL_FW_INS_CMD) ; \
	else \
		$(MSG11) "Kernel firmware archive is missing:" $(EMSG); \
		$(MSG11) "$(KERNEL_ARCDIR)/$(KERNEL_FW_VERSION) not found " $(EMSG); \
		exit 1; \
	fi
	@for fw in $(fwobj); do \
		dir=$$(echo $$fw | cut -f1 -d":"); \
		url=$$(echo $$fw | cut -f2- -d":"); \
		filename=$$(echo $$fw | cut -f3 -d":"); \
		filename=$$(basename $$filename); \
		$(MSG3) "Retrieving $$url to $$dir" $(EMSG); \
		mkdir -p $(KERNEL_SRCDIR)/modules/lib/firmware/$$dir; \
		rm -f $(KERNEL_SRCDIR)/modules/lib/firmware/$$dir/$$filename; \
		cd $(KERNEL_SRCDIR)/modules/lib/firmware/$$dir && wget "$$url"; \
	done
	@for fw in $(kfwobj); do \
		objname=$$(basename $$fw); \
		dirname=$$(echo $$fw | sed 's%'$$objname'%%'); \
		$(MSG3) "Copying $$fw from Linux firmware repo" $(EMSG); \
		mkdir -p $(KERNEL_SRCDIR)/modules/lib/firmware/$$dirname; \
		cp $(KERNEL_ARCDIR)/$(KERNEL_FW_VERSION)/"$$fw" $(KERNEL_SRCDIR)/modules/lib/firmware/$$dirname; \
	done
	@cp -r $(DIR_KERNEL)/firmware/* $(KERNEL_SRCDIR)/modules/lib/firmware/

# Build some kernel modules manually.
# Must be built after the kernel is built.
.$(KERNEL_T)-build-drivers $(KERNEL_T)-build-drivers:
	@for drvr in $(drbld); do \
		$(MSG3) "Buildring $$drvr" $(EMSG); \
		make --no-print-directory $$drvr-build; \
	done

# Build the kernel
$(KERNEL_T): .$(KERNEL_T)

.$(KERNEL_T): .$(KERNEL_T)-init 
	@make --no-print-directory $(KERNEL_T)-verify
	@$(MSG) "================================================================"
	@$(MSG2) "Building Linux Kernel for $(HW)" $(EMSG)
	@$(MSG) "================================================================"
	@make --no-print-directory $(KERNEL_T)-preconfig
	@cd $(KERNEL_SRCDIR) && PATH=$(KERNEL_PATH) make -j$(JOBS) ARCH=$(ARCH) CROSS_COMPILE=$(CROSS_COMPILER) $(KERNEL_TARGET) 
ifeq ($(GEN_DTBS),1)
	@cd $(KERNEL_SRCDIR) && PATH=$(KERNEL_PATH) make -j$(JOBS) ARCH=$(ARCH) CROSS_COMPILE=$(CROSS_COMPILER) dtbs
endif
	@cd $(KERNEL_SRCDIR) && PATH=$(KERNEL_PATH) make -j$(JOBS) ARCH=$(ARCH) CROSS_COMPILE=$(CROSS_COMPILER) \
		INSTALL_MOD_PATH=$(KERNEL_SRCDIR)/modules modules 
	@cd $(KERNEL_SRCDIR) && PATH=$(KERNEL_PATH) make -j$(JOBS) ARCH=$(ARCH) CROSS_COMPILE=$(CROSS_COMPILER) \
		INSTALL_MOD_PATH=$(KERNEL_SRCDIR)/modules modules_install 
	@cd $(KERNEL_SRCDIR) && PATH=$(KERNEL_PATH) make -j$(JOBS) ARCH=$(ARCH) CROSS_COMPILE=$(CROSS_COMPILER) \
		INSTALL_HDR_PATH=$(KERNEL_SRCDIR)/headers headers_install
	@make --no-print-directory $(KERNEL_T)-build-drivers
	@make --no-print-directory $(KERNEL_T)-firmware
	@if [ -f $(KERNEL_SRCDIR)/arch/$(ARCH)/boot/$(KERNEL_IMAGE) ]; then \
		$(MSG6) "Built Linux Kernel:" $(EMSG); \
	else \
		$(MSG11) "Kernel build completed but can't find $(KERNEL_IMAGE):" $(EMSG); \
		exit 1; \
	fi
	@ls -l $(KERNEL_SRCDIR)/arch/$(ARCH)/boot/$(KERNEL_IMAGE)
ifeq ($(GEN_DTBS),1)
	@ls -l $(KERNEL_SRCDIR)/$(KERNEL_DTS_DIR)/$(KERNEL_DTB)
endif
	@touch .$(subst .,,$@)

# Edit the Kernel configuration through its menuconfig utility
$(KERNEL_T)-menuconfig: .$(KERNEL_T)-init
	@cd $(KERNEL_SRCDIR) && PATH=$(KERNEL_PATH) make ARCH=$(ARCH) CROSS_COMPILE=$(CROSS_COMPILER) menuconfig

$(KERNEL_T)-meld:
	@meld $(KERNEL_SRCDIR)/.config $(KERNEL_CONFIG)

$(KERNEL_T)-diff:
	@- diff -u $(KERNEL_SRCDIR)/.config $(KERNEL_CONFIG)

# Save the Kernel configuration as a template in our source tree.
$(KERNEL_T)-saveconfig: 
	@if [ -f $(KERNEL_SRCDIR)/.config ]; then \
		$(MSG) "================================================================"; \
		$(MSG2) "Saving Kernel configuration" $(EMSG); \
		$(MSG) "================================================================"; \
		cp $(KERNEL_SRCDIR)/.config $(KERNEL_SAVECFG); \
	else \
		$(MSG3) "There is no Kernel configuration to save." $(EMSG); \
	fi

$(KERNEL_T)-cscope:
	@$(MSG3) "Generating CSCOPE database for kernel." $(EMSG)
	@cd $(KERNEL_SRCDIR) && PATH=$(KERNEL_PATH) make ARCH=$(ARCH) CROSS_COMPILE=$(CROSS_COMPILER) cscope
	@cd $(KERNEL_SRCDIR) && cscope
	@$(MSG3) "Done." $(EMSG)

$(KERNEL_T)-files:
	@$(MSG) "================================================================"
	@$(MSG2) "$(KERNEL_T) artifacts" $(EMSG)
	@$(MSG) "================================================================"
	@ls -l $(KERNEL_SRCDIR)/arch/$(ARCH)/boot/uImage 
ifeq ($(GEN_DTBS),1)
	@ls -l $(KERNEL_SRCDIR)/$(KERNEL_DTS_DIR)/$(KERNEL_DTB)
endif

$(KERNEL_T)-pkg: .$(KERNEL_T) $(PKG_T)-init
	@$(MSG3) "Gathering for $(KERNEL_T)" $(EMSG)
	@if [ -f $(KERNEL_SRCDIR)/arch/$(ARCH)/boot/$(KERNEL_TARGET) ]; then \
		cp $(KERNEL_SRCDIR)/arch/$(ARCH)/boot/$(KERNEL_TARGET) $(PKGDIR)/kernel.img; \
	else \
		$(MSG11) "Missing kernel image: $(KERNEL_SRCDIR)/arch/$(ARCH)/boot/$(KERNEL_TARGET)" $(EMSG); \
	fi
ifeq ($(GEN_DTBS),1)
	@if [ -f $(KERNEL_SRCDIR)/$(KERNEL_DTS_DIR)/$(KERNEL_DTB) ]; then \
		cp $(KERNEL_SRCDIR)/$(KERNEL_DTS_DIR)/$(KERNEL_DTB) $(PKGDIR)/$(KERNEL_DTB); \
	else \
		$(MSG11) "Missing DTB image: $(KERNEL_SRCDIR)/$(KERNEL_DTS_DIR)/$(KERNEL_DTB)" $(EMSG); \
	fi
endif

$(KERNEL_T)-clean: $(KERNEL_T)-clean-drivers
	@if [ -d $(KERNEL_SRCDIR) ]; then cd $(KERNEL_SRCDIR) && make clean; fi
	@rm -f .$(KERNEL_T)

$(KERNEL_T)-clean-drivers: 
	@for drvr in $(drbld); do \
		$(MSG3) "Cleaning $$drvr" $(EMSG); \
		make --no-print-directory $$drvr-clean; \
	done

# Remove everything except the archives - allows restarting without downloading.
$(KERNEL_T)-distclean: 
	@if [ -d $(KERNEL_SRCDIR) ]; then rm -rf $(KERNEL_SRCDIR); fi
	@rm -f .$(KERNEL_T) .$(KERNEL_T)-init .$(KERNEL_T)-unpack .$(KERNEL_T)-unpack-patches .$(KERNEL_T)-patch

$(KERNEL_T)-clobber:
	@if [ -d $(KERNEL_SRCDIR) ]; then rm -rf $(KERNEL_SRCDIR); fi
	@make --no-print-directory -i $(KERNEL_T)-clean
	@rm -f .$(KERNEL_T)-init .$(KERNEL_T)-get .$(KERNEL_T)-get-patches .kernel-get-drivers \
		.$(KERNEL_T)-unpack .$(KERNEL_T)-unpack-patches \
		.$(KERNEL_T)-patch

