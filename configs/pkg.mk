# ---------------------------------------------------------------
# Package everything.
# ---------------------------------------------------------------
$(PKG_T)-init: 
	@mkdir -p $(PKGDIR)

$(PKG_T): $(PKG_T)-src $(PKG_T)-scripts 
	@rm -f $(PKGDIR)/pibox-*.tar.gz
	@$(MSG) "================================================================"
	@$(MSG2) "Gathering package files" $(EMSG)
	@$(MSG) "================================================================"
	@for component in $(PKG_TARGETS); do \
		if [ "$$component" != "$(PKG_T)" ]; then \
			make --no-print-directory $$component-pkg; \
		fi; \
	done
	@$(MSG) "================================================================"
	@$(MSG2) "Package List" $(EMSG)
	@$(MSG) "================================================================"
	mv $(PKGDIR) $(TOPDIR)/pibox-$(PKG_VERSION)
	cd $(TOPDIR) && tar -cvzf pibox-$(PKG_VERSION).tar.gz pibox-$(PKG_VERSION)
	mv $(TOPDIR)/pibox-$(PKG_VERSION) $(PKGDIR) 
	mv $(TOPDIR)/pibox-$(PKG_VERSION).tar.gz $(PKGDIR)/
	@ls -lR $(PKGDIR)

$(PKG_T)-src: $(PKG_T)-init
	rm -f $(PKGDIR)/pibox-*-src.tar.gz
	mkdir $(PKGDIR)/pibox-$(PKG_VERSION)
	cd $(PKGDIR) && git clone git@gitlab.com:pibox/pibox.git pibox-$(PKG_VERSION)
	cd $(PKGDIR) && tar --exclude=.git -cvzf pibox-$(PKG_VERSION)-src.tar.gz pibox-$(PKG_VERSION)
	rm -rf $(PKGDIR)/pibox-$(PKG_VERSION)

# ---------------------------------------------------------------
# Component specific packaging 
# ---------------------------------------------------------------
scripts-$(PKG_T): $(PKG_T)-scripts

$(PKG_T)-scripts: $(PKG_T)-init
	@$(MSG) "================================================================"
	@$(MSG2) "Grabbing installation scripts" $(EMSG)
	@$(MSG) "================================================================"
	@cp $(SCRIPTDIR)/mksd.sh.$(PROJECT) $(PKGDIR)/mksd.sh
	@cp $(SCRIPTDIR)/mkinstall.sh.$(PROJECT) $(PKGDIR)/mkinstall.sh
	@cp $(SCRIPTDIR)/qemu.sh $(PKGDIR)
ifeq ($(HW),rpi)
	@sed -i 's%^DESTUIMAGE=.*%DESTUIMAGE=\"kernel.img\"%' $(PKGDIR)/mkinstall.sh
endif
ifeq ($(HW),rpi2)
	@sed -i 's%^DESTUIMAGE=.*%DESTUIMAGE=\"kernel7.img\"%' $(PKGDIR)/mkinstall.sh
endif
ifeq ($(PROJECT),xeon)
	@cp $(SCRIPTDIR)/extlinux.conf $(PKGDIR)
	@sed -i 's%^DESTUIMAGE=.*%DESTUIMAGE=\"kernel.img\"%' $(PKGDIR)/mkinstall.sh
endif

$(PKG_T)-clean:
	@rm -rf $(PKGDIR) 

$(PKG_T)-clobber: $(PKG_T)-clean

