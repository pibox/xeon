%define rev 1
%define ppcdir opt/[--PKGPRJ--]TC

# Disabled automatic stripping of binaries by RPM.
%define __os_install_post %{nil}

Name: [--PKGPRJ--]Toolchain
Summary: PiBox Cross Toolchain
Version: [--VERSION--]
Release: %{rev}
Source: %{name}-%{version}.tar.gz
Vendor: The PiBox Project
License: MIT
URL:  http://www.graphics-muse.org/wiki/pmwiki.php/
Packager: Michael J. Hammel <mjhammel@graphics-muse.org>

# Group is based on the Fedora Groups hierarchy.  See
# /usr/share/doc/rpm-<version>/GROUPS for details.
Group: Development/Tools

AutoReqProv: no
BuildRoot: %{_tmppath}/%{name}-root

%description
This package contains a cross toolchain for use in
building software for the PiBox project.

%prep
%setup
                                                                                
%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{ppcdir}
cp -r [--PKGPRJ--]TC/* $RPM_BUILD_ROOT/%{ppcdir}

%clean
rm -rf $RPM_BUILD_ROOT

%preun

%files
%attr(0755,root,root) /%{ppcdir}
