#############################################################
#
# libpibox
#
#############################################################
LIBPIBOX_VERSION = master
LIBPIBOX_SOURCE = libpibox-$(LIBPIBOX_VERSION).tar.gz
LIBPIBOX_SITE = https://gitlab.com/pibox/libpibox/-/archive/$(LIBPIBOX_VERSION)
LIBPIBOX_DEPENDENCIES = host-pkgconf libgtk2
LIBPIBOX_AUTORECONF = YES
LIBPIBOX_LIBTOOL_PATCH = YES
LIBPIBOX_INSTALL_STAGING = YES
LIBPIBOX_LICENSE_FILES = COPYING

define LIBPIBOX_PRE_CONFIGURE_FIXUP
    cd $(@D) && autoreconf -i
endef

LIBPIBOX_PRE_CONFIGURE_HOOKS += LIBPIBOX_PRE_CONFIGURE_FIXUP

define LIBPIBOX_POST_BUILD_TOOLS
    cd $(@D) && make tools
    cd $(@D) && cp ptools/pbtouchstate $(TARGET_DIR)/usr/bin
    cd $(@D) && cp ptools/pbsetentropy $(TARGET_DIR)/usr/bin
    cd $(@D) && cp ptools/pbeventlist $(TARGET_DIR)/usr/bin
endef

LIBPIBOX_POST_BUILD_HOOKS += LIBPIBOX_POST_BUILD_TOOLS

$(eval $(autotools-package))
