#############################################################
#
# nldev
#
#############################################################
# NLDEV_VERSION:=master
# NLDEV_SITE:=git://git.r-36.net/nldev

NLDEV_VERSION = 20180722
NLDEV_SITE = https://www.graphics-muse.org/archives/pibox/mirror/nldev
NLDEV_SOURCE = nldev-$(NLDEV_VERSION).tar.gz

# Modify the config.mk file in the source for proper 
# compilation and installation.
define NLDEV_CONFIGURE_CMDS
	cd $(@D) && \
 		sed -i 's%^INCS =.*%INCS = -I. -I$(STAGING_DIR)usr/include%' config.mk && \
		sed -i 's%^LIBS =.*%LIBS = -L$(STAGING_DIR)/usr/lib -lc%' config.mk 
endef

define NLDEV_BUILD_CMDS
 	$(MAKE) CC=$(TARGET_CC) LD=$(TARGET_LD) -C $(@D) 
endef

# Don't overwrite existing mdev.conf - user may have added their own previously.
define NLDEV_INSTALL_TARGET_CMDS
	$(MAKE) CC=$(TARGET_CC) LD=$(TARGET_LD) PREFIX=$(TARGET_DIR)/usr -C $(@D) install 
	if [ ! -f $(TARGET_DIR)/etc/mdev.conf ]; then \
		cp $(@D)/mdev/etc/mdev.conf $(TARGET_DIR)/etc/mdev.conf; \
	fi
	mkdir -p $(TARGET_DIR)/lib/mdev
	cp $(@D)/mdev/lib/* $(TARGET_DIR)/lib/mdev/
endef

define NLDEV_CLEAN_CMDS
	$(MAKE) CC=$(TARGET_CC) LD=$(TARGET_LD) -C $(@D) clean
endef

define NLDEV_UNINSTALL_CMDS
	rm $(TARGET_DIR)/usr/bin/{nldev,nltrigger,run_nldev} && \
	rm $(TARGET_DIR)/usr/share/man/man8/nldev.8 && \
	rm -rf $(TARGET_DIR)/lib/mdev/ && \
	rm -rf $(TARGET_DIR)/etc/mdev.conf && \
	if [ -f $(TARGET_DIR)/etc/mdev.conf.orig ]; then \
		mv $(TARGET_DIR)/etc/mdev.conf.orig $(TARGET_DIR)/etc/mdev.conf; \
	fi
endef

$(eval $(generic-package))
