#!/bin/sh

# Only argument is path to target directory
TARGET=$1

# Source for customizations
SRC="[CUSTOMSRC]"
LEGALSRC="[LEGALSRC]"

# Copy customizations into target tree
rsync -ar "${SRC}"/* "${TARGET}/"

# Remove sshd init script, if it exists.
# We use dropbear instead, but OpenSSH adds init script.
rm -f "${TARGET}/etc/init.d/S50sshd"

# Add missing /proc and /sys directories
mkdir -p "${TARGET}/proc"
mkdir -p "${TARGET}/sys"

# Add missing /media directory
mkdir -p "${TARGET}/media"

# Setup opkg configuration and runtime directories
mkdir -p "${TARGET}/etc/opkg/db"
mkdir -p "${TARGET}/etc/opkg/info"
mkdir -p "${TARGET}/etc/opkg/lists"
mkdir -p "${TARGET}/etc/opkg/unpack"

# Remove dhcpd-server init script. S40network handles that instead.
rm -f "${TARGET}/etc/init.d/S80dhcp-server"

# Copy dhcpd.conf into two spots so we know it gets found,
# no matter which build we're doing.
cp "${TARGET}/etc/dhcpd.conf" "${TARGET}/etc/dhcp/dhcpd.conf"

# Remove S40xorg - it comes with Buildroot's X.org and we don't need it.
rm -f "${TARGET}/etc/init.d/S40xorg"

# Make the directory where cron configurations can be stored.
mkdir -p "${TARGET}/var/spool/cron"
chmod 755 "${TARGET}/var/spool"
chmod 700 "${TARGET}/var/spool/cron"

# Generate an RNG seed file
dd if=/dev/urandom of="${TARGET}/etc/random-seed" count=1 bs=512

# Install legal files to target.
# Can't do this in the build due to ordering issues.
if [ -d "${LEGALSRC}" ]; then
	mkdir -p "${TARGET}"/usr/share/
	cp -r "${LEGALSRC}" "${TARGET}"/usr/share/
	find "${TARGET}"/usr/share/legal-info -type f -exec chmod 644 {} \;
	find "${TARGET}"/usr/share/legal-info -type d -exec chmod 755 {} \;
else
	echo "Can't find Buildroot Legal Info files"
fi

# Somehow we end up with some staging files in the /home/<user> tree.
# Until we figure how this happens, let's just clear them here.
for dir in "${TARGET}"/home/*; do
    rm -rf "${dir}"
done

