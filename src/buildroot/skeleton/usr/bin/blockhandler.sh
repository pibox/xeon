#!/bin/sh
# Automounter for removable devices under mdev
# Based on block.agent by snafu [Alexander Zangerl (az)]
#     http://www.snafu.priv.at/mystuff/mdev/block.agent
# License: GPL version 1 or version 2
# -------------------------------------------------------------------
# This script mounts under at /media/usb.
# It ignores non-removable devices.
# It unmounts if the device is removed.
# Mount points are created and removed as necessary.
# Requirements: busybox and mdev.
# OPTIONFILE (/etc/block.settings) format:
# <UUID> <Label> <fstype> <mount options>
# Examples:
# * * ext3 ro
# * * ext4 ro
# -------------------------------------------------------------------
# Note:
# We're always mounting read-only since we expect there to be
# media for playback on the removable device.
# -------------------------------------------------------------------
DEBUG=0
# -------------------------------------------------------------------
# LOGGING setup and functions
# -------------------------------------------------------------------
# Debug enabled and terminal on stderr? Yes: log to terminal and to syslog, otherwise syslog only
ME=$(busybox basename "$0")"[$$]"
[ -t 2 ] && [ ${DEBUG} -eq 1 ] && LOGARGS=-s
mesg () 
{
    busybox logger "${LOGARGS}" -t "${ME}" "$@"
}

debug_mesg () 
{
    # [ -z "${DEBUG}" ] && return
    mesg "$@"
}

# -------------------------------------------------------------------
# Initialization
# -------------------------------------------------------------------
BASE=/media
STATEF=/var/run/block.agent.state
OPTIONFILE="/etc/block.settings"
# shellcheck disable=SC2164
cd /tmp

# -------------------------------------------------------------------
# Initialization
# -------------------------------------------------------------------
# Finds a mountpoint for specified device (passed as $1)
# Sets $MOUNT to the full mount command, or empty if not to be mounted.
# Sets $TARGET to the mount point.
findmount ()
{
    RD=$1
    MOUNT=""

    debug_mesg "testing ${RD} for filesystem"

    # This loads the output from blkid into the environment 
    ID_FS_TYPE="$(blkid "${RD}" | sed 's/^.*TYPE=//' | sed 's/"//g')"

    # Make sure we have a filesystem - ignore if not.
    # shellcheck disable=SC2181
    if [ $? != 0 ] || [ -z "${ID_FS_TYPE}" ]; then
        debug_mesg "${RD} has no detectable filesystem"
        return
    fi

    # Use the device name as the mount point if it's
    # an mmc device, otherwise append "usb" to the path.
    DNAME="$(busybox basename "${RD}")"
    if echo "${DNAME}" | grep mmc >/dev/null 2>&1; then
        REALBASE="${BASE}"
    else
        REALBASE="${BASE}/usb"
    fi
    TARGET="${REALBASE}/${DNAME}"
    # PREF="${DNAME}"

    MOUNTOPT="-"
    # determine where, how and if to mount this thing
    # shellcheck disable=SC2162
    while read U L T O ; do
        [ "$(printf %.1s "${U}")" = "#" ] && continue;
        debug_mesg "testing  ${ID_FS_UUID} ${ID_FS_LABEL} ${ID_FS_TYPE} against ${U} ${L} ${T}"

        # shellcheck disable=SC2166
        if [ \( "${U}" = "${ID_FS_UUID}" -o "${U}" = "*" \) \
            -a \( "${L}" = "${ID_FS_LABEL}" -o "${L}" = "*" \) \
            -a \( "${T}" = "${ID_FS_TYPE}" -o "${T}" = "*" \) ] ; then
            debug_mesg "found entry ${U} ${L} ${T} ${O} for ${RD}";
            MOUNTOPT="${O}";
            break;
        fi
    done < "${OPTIONFILE}"

    if [ "${MOUNTOPT}" = "-" ] ; then
        mesg "${RD}: no options available for automounting - skipping."
        return
    else
        mesg "${RD}: ${TARGET} options ${MOUNTOPT}"
        if [ -n "${MOUNTOPT}" ] ; then
            MOUNT="mount -o ${MOUNTOPT} ${RD} ${TARGET}"
        else
            MOUNT="mount ${RD} ${TARGET}"
        fi
    fi
    return
}

# -------------------------------------------------------------------
# Mount a device
# -------------------------------------------------------------------
add()
{
    # sets $MOUNT, $TARGET
    findmount "${REALDEV}"
    if [ -z "${MOUNT}" ] ; then
        exit 0;
    else
        mesg "trying automount of ${REALDEV} at ${TARGET}"
        [ ! -d "${TARGET}" ] && mkdir -p "${TARGET}"
        debug_mesg "${MOUNT}"
        # shellcheck disable=SC2086
        eval ${MOUNT}
        RES=$?
        if [ ${RES} != 0 ] ; then
            mesg "mounting ${REALDEV} failed with ${RES}"
            exit 1
        fi
        echo "${REALDEV} ${TARGET}" >> "${STATEF}"
    fi
}

# -------------------------------------------------------------------
# Handle mmc devices, but only those we care about.
# -------------------------------------------------------------------
mmc ()
{
    # Handle mmcblk0p[12] partitions
    PREFIX="$(echo "${MDEV}" | grep -E "^mmcblk0p")"
    if [ "${PREFIX}" != "" ]
    then
        # Add the device
        add
    fi
}

# -------------------------------------------------------------------
# Choose an action based on what mdev is handling us.
# -------------------------------------------------------------------
# what we get: 
# ACTION=add HOME=/ SEQNUM=2223 PWD=/dev 
# DEVNAME=sdd1 MAJOR=8 MINOR=49 DEVTYPE=partition
# that's from mdev: MDEV=sdd1 SUBSYSTEM=block
# DEVPATH=/devices/pci0000:00/0000:00:1d.7/usb1/1-7/1-7:1.0/host3/target3:0:0/3:0:0:0/block/sdd/sdd1

REALDEV="/dev/${MDEV}"
debug_mesg "DEVNAME: ${DEVNAME}"
debug_mesg "REALDEV: ${REALDEV}"
debug_mesg "DEVPATH: ${DEVPATH}"

if [ "${ACTION}" = "add" ] || [ "${ACTION}" = "change" ]; then

    if [ "${DEVPATH}" != "" ]
    then
        # Test if the device is a removable device.
        REMF="/sys/${DEVPATH}/removable"
        [ -f "${REMF}" ] || REMF="/sys/${DEVPATH}/../removable"
        if [ ! -f "${REMF}" ]; then
            debug_mesg "No /sys/.../removable path, trying mmc mount."
    	    mmc
            exit 0
        fi

        if [ "$(cat "${REMF}")" != 1 ]; then
            debug_mesg "device ${REALDEV} is not removable."
            exit 0
        fi
    fi

    # Add the device
    debug_mesg "Trying to add ${MDEV}"
    add

elif [ "${ACTION}" = "remove" ] ; then
    MPT=$(grep -E "^${REALDEV} /" ${STATEF}|cut -f 2 -d " ")

    # ours? unmount if mounted, remove dir
    if [ -n "${MPT}" ]; then

        grep -E -v "^${REALDEV} ${MPT}" "${STATEF}" > "${STATEF}.x"
        mv "${STATEF}.x" "${STATEF}"

        LOC=$(grep -E "^${REALDEV} ${MPT}" /proc/mounts)
        if [ -n "${LOC}" ] ; then
            mesg "attempting late unmount of ${REALDEV} from ${MPT}"
            umount -f "${MPT}"
            RES=$?
            if [ ${RES} != 0 ] ; then
                mesg "unmounting ${MPT} failed with ${RES}"
            fi
        fi
        mesg "cleaning up mountpoint dir ${MPT} for ${REALDEV}"
        rmdir "${MPT}"
    fi
elif [ "${ACTION}" = "" ] ; then
    # No action - this may be a boot time reference
    # We only care about mmcblk0p[12] partitions
    PREFIX=$(echo "${MDEV}" | grep -E "^mmcblk0p")
    if [ "${PREFIX}" != "" ]
    then
        # Add the device
        add
    else
        debug_mesg "ignoring ${MDEV} (no action)"
        exit 1
    fi
else
    debug_mesg "ignoring action ${ACTION} with ${MDEV}"
    exit 1
fi
exit 0
