#!/bin/sh
# AVailable event variables:
#
# ACTION
# SEQNUM
# MAJOR
# MDEV
# DEVPATH
# SUBSYSTEM
# MINOR
# PHYSDEVPATH
# PHYSDEVDRIVER
# PHYSDEVBUS
# PWD
#-----------------------------------------------
FD=/var/log/mdevdebug.log
(
cat<<EOF
New Event
    ACTION      = $ACTION
    SEQNUM      = $SEQNUM
    MAJOR       = $MAJOR
    MDEV        = $MDEV
    DEVPATH     = $DEVPATH
    DEVNAME     = $DEVNAME
    SUBSYSTEM   = $SUBSYSTEM
    MINOR       = $MINOR
    PHYSDEVPATH = $PHYSDEVPATH
    PHYSDEVBUS  = $PHYSDEVBUS
    PWD         = $PWD
----------------------------------
EOF
) >> $FD

