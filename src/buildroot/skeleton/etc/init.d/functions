#!/bin/sh
# Utility functions for all init scripts
# --------------------------------------
# shellcheck disable=SC2034
MODDEP=/lib/modules/"$(uname -r)"/modules.dep
CONFIG=/media/mmcblk0p1/config.txt
LOGDIR=/media/mmcblk0p3/log
PIBOXCONFIG=/etc/pibox-config
ECHO="echo"
ENABLE_LOG=1
FRAMEBUFFER="/dev/fb0"

# Append message to kmsg and print to stdout.
log()
{
    if [ ${ENABLE_LOG} -eq 1 ]; then
        echo "$1" | tee /dev/kmsg
    else
        echo "$1" > /dev/kmsg
    fi
}

# Get hardware model information.
getModel()
{
    cat /sys/firmware/devicetree/base/model
}

# Return the number of init scripts.
numScripts()
{
    DIR=/etc/init.d
    CWD="$(pwd)"
    # shellcheck disable=SC2164
    cd "${DIR}"
    cnt=0
    for file in S??*
    do
        val="$(echo "${file}" | cut -c2-3)"
        # shellcheck disable=SC2086
        if [ ${val} -lt 90 ]
        then
            cnt=$(( cnt + 1 ))
        fi
    done
    # shellcheck disable=SC2164
    cd "${CWD}"
    echo "${cnt}"
}

# Return the value for a specified kernel
# command line argument. If not found, return
# an empty string.
getArgValue()
{
    ARG=$1
    VALUE=
    # shellcheck disable=SC2013
    for i in $(cat /proc/cmdline)
    do
        echo "${i}" | grep "$ARG" > /dev/null
        # shellcheck disable=SC2181
        if [ $? = 0 ]; then
            VALUE="$(echo "${i}" | cut -f2 -d"=")"
            break
        fi
    done
    echo "${VALUE}"
}

# Generate the base pibox-config file.
genConfig()
{
    # Grab display settings
    STR="$( /opt/vc/bin/tvservice -s )"
    STR=${STR#*[}
    STR=${STR%]*}
    echo "$STR" > "${PIBOXCONFIG}"
    /opt/vc/bin/tvservice -s | cut -f2 -d"," | cut -f2 -d" " >> "${PIBOXCONFIG}"

    echo "PiBox Config:"
    cat "${PIBOXCONFIG}"
}

# If this is a known touch screen then optimize for it.
optimizeTouchScreen()
{
    touchscreen="$( pbtouchstate )"
    if [ "${touchscreen}" != "No touchscreen found" ]
    then
        echo "RPITOUCH" >> "${PIBOXCONFIG}"

        if [ -e "${CONFIG}" ]; then
            case "${touchscreen}" in
                "FT5406 memory based driver"|"raspberrypi-ts")
                    # Update for an official Raspbery Pi LCD.  This allows
                    # omxplayer, for example, to play videos without jitter.
                    log "Updating the config file for use with LCD."
                    (
                        echo ""
                        echo "# Raspberry Pi Touchscreen setup"
                        echo "dtoverlay=rpi-ft5406-overlay"
                        echo "lcd_rotate=2"
                        echo "framebuffer_width=$width"
                        echo "framebuffer_height=$height"
                        echo "fbwidth=$width"
                        echo "fbheight=$height"

                        # These are LCD display optimizations.
                        echo "dmi_ignore_edid=0xa5000080"
                        echo "config_hdmi_boost=7"
                        echo "gpu_mem=320"
                        echo "hdmi_group=2"
                        echo "hdmi_mode=87"
                        echo "hdmi_cvt=800 480 60 6 0 0 0"
                        echo "hdmi_drive=1"
                    ) >> "${CONFIG}"
                    ;;
                *)
                    # Disable the RPi touchscreen drivers - we won't need them.
                    log "This is not an official RPi touchscreen."
                    sed -i 's/^rpi-ft5406/# rpi-ft5406/' /etc/modules.conf
                    sed -i 's/^rpi_backlight/# rpi_backlight/' /etc/modules.conf
                    ;;
            esac
        else
            log "Can't find $CONFIG - no updates made for touch screen."
        fi
    else
        echo "NOTOUCH" >> "${PIBOXCONFIG}"
        log "This doesn't appear to be a touchscreen display."
    fi
}

# If this is a known touch screen then optimize for it.
# We only care about the following, currently.
# Model and PCB Revision    RAM     Revision    Pi Revision Code from cpuinfo
# Pi 2 Model B v1.1         1GB                 a01041 (Sony, UK)
# Pi 2 Model B v1.1         1GB                 a21041 (Embest, China)
# Pi 2 Model B v1.2         1GB     1.2         a22042
# Pi Zero v1.2              512MB   1.2         900092
# Pi Zero v1.3              512MB   1.3         900093
# Pi Zero W                 512MB   1.1         9000C1
# Pi 3 Model B              1GB     1.2         a02082 (Sony, UK)
# Pi 3 Model B              1GB     1.2         a22082 (Embest, China)
# Pi 3 Model B+             1GB     1.3         a020d3 (Sony, UK)
# See: https://www.raspberrypi-spy.co.uk/2012/09/checking-your-raspberry-pi-board-version/
# Note that once these settings are applied it is probably not safe to move the SD card
# to different model boards.
optimizeBoard()
{
    # Get the board revision from CPU info
    revision=$(cat /proc/cpuinfo | grep Revision | cut -f2 -d":" | tr -d '[:space:]')
    length=$(echo "${revision}" | wc -c)
    if [ ${length} -gt 6 ]; then
        offset=$(( length - 6 ))
        revision=$(echo "${revision}" | cut -c${offset}-)
    fi
    if [ -z "${revision}" ]; then
        revision="Unknown"
    fi

    echo "Board Revision: ${revision}"
    case "${revision}" in
    
        # Overclocing of RPi2.
        # https://haydenjames.io/raspberry-pi-2-overclock/
        a01041|a21041|a22042)
            # Remove any previous overclocking.
            sed -i '/^# Raspbery Pi 2 optimizations/d' "${CONFIG}"
            sed -i '/^force_turbo/d' "${CONFIG}"
            sed -i '/^boot_delay/d' "${CONFIG}"
            sed -i '/^arm_freq/d' "${CONFIG}"
            sed -i '/^sdram_freq/d' "${CONFIG}"
            sed -i '/^core_freq/d' "${CONFIG}"
            sed -i '/^over_voltage/d' "${CONFIG}"
            sed -i '/^temp_limit/d' "${CONFIG}"

            # Note: "force_turbo" will void warranty.
            log "Optimizing config file for use with RPi2."
            (
                echo ""
                echo "# Raspberry Pi 2 optimizations"
                echo "force_turbo=1"
                echo "boot_delay=1"
                echo "arm_freq=1000"
                echo "sdram_freq=500"
                echo "core_freq=500"
                echo "over_voltage=2"
                echo "temp_limit=80"
            ) >> "${CONFIG}"
            ;;

        # Overclocing of PiZero is not required.
        900092|900093|9000C1)
            ;;

        # Overclocing of RPi3.
        # https://haydenjames.io/raspberry-pi-3-overclock/
        a02082|a22082|a020d3)
            # Remove any previous overclocking.
            sed -i '/^# Raspbery Pi 3 optimizations/d' "${CONFIG}"
            sed -i '/^force_turbo/d' "${CONFIG}"
            sed -i '/^arm_freq/d' "${CONFIG}"
            sed -i '/^core_freq/d' "${CONFIG}"
            sed -i '/^over_voltage/d' "${CONFIG}"

            # Note: "force_turbo" will void warranty.
            log "Optimizing config file for use with RPi3."
            (
                echo ""
                echo "# Raspberry Pi 3 optimizations"
                # RPi3 overclocking optimizations
                echo "force_turbo=1"
                echo "arm_freq=1400"
                echo "core_freq=500"
                echo "over_voltage=4"
            ) >> "${CONFIG}"

            ;;

        # Other boards are not yet supported.
        *)
            log "This board is not supported by config.txt optimizations."
            ;;
    esac
}

# Test if the init processing should be quiet or verbose.
getQuiet()
{
    local term="quiet"
    local kernel_args
    kernel_args="$(cat /proc/cmdline)"
    if ! $(echo "${kernel_args}"|grep -q "${term}") ; then
        ECHO="/bin/true"
        ENABLE_LOG=0
    fi
}
getQuiet

# Load functions from add on packages
# Some of the functions may depend on these functions
# so load them after these functions.
if [ -d /etc/init.d/functions.d ]
then
    # shellcheck disable=SC2012
    numFiles="$(ls -1 /etc/init.d/functions.d/* | wc -l)"
    # shellcheck disable=SC2086
    if [ ${numFiles} -gt 0 ]
    then
        for file in /etc/init.d/functions.d/*
        do
            # shellcheck disable=SC1090
            . "${file}"
        done
    fi
fi

