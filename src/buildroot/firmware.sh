#!/bin/bash
# List of firwmare to download.
# Add new entries by first checking for the existance of the file!
# Note: First arg is the location of the top of the target directory.
# ----------------------------------------------------------------
function msg 
{
	echo "...${1}"
}
function die 
{
	msg "${1}"
    exit 1
}

FWDIR=lib/firmware

# The arg to this script is the location of the top of the target
# directory.  Firmware files are stored in $TARGET/$FWDIR
if [[ ! -d "${1}" ]]
then
    echo "$0: Can't find target directory: $1"
    exit 1
fi
TARGET="${1}"
cd "${TARGET}" || die "Can't change directory to ${TARGET}"

# Create the firmware directory.
mkdir -p "${FWDIR}"
cd "${FWDIR}" || die "Can't change directory to ${FWDIR}"
echo "Retrieving firmware..."

# ----------------------------------------------------------------
# Add firmware downloads here.
# ----------------------------------------------------------------

# AR9170 (eg: TP-Link TL-WN8120N)
if [[ ! -f ar9170.fw ]]; then msg ar9170.fw; wget -q http://wireless.kernel.org/en/users/Drivers/ar9170.fw; fi


# ----------------------------------------------------------------
# Don't change below here!
# ----------------------------------------------------------------
echo "Firmware downloads completed."
