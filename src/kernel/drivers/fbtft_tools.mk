FBTFT_TOOLS     = fbtft_tools
FBTFT_TOOLS_URL = https://github.com/notro/fbtft_tools.git
FBTFT_TOOLS_DIR = $(KERNEL_SRCDIR)/drivers/video
ADS7846_DIR     = $(FBTFT_TOOLS_DIR)/$(FBTFT_TOOLS)/ads7846_device
GPIOK_DIR       = $(FBTFT_TOOLS_DIR)/$(FBTFT_TOOLS)/gpio_keys_device
GPIOM_DIR       = $(FBTFT_TOOLS_DIR)/$(FBTFT_TOOLS)/gpio_mouse_device
GPIOR_DIR       = $(FBTFT_TOOLS_DIR)/$(FBTFT_TOOLS)/rpi_power_switch
STMPE_DIR       = $(FBTFT_TOOLS_DIR)/$(FBTFT_TOOLS)/stmpe_device

# This one doesn't build
GPIOB_DIR       = $(FBTFT_TOOLS_DIR)/$(FBTFT_TOOLS)/gpio_backlight_device

$(FBTFT_TOOLS)-get:
	@if [ ! -d $(FBTFT_TOOLS_DIR)/$(FBTFT_TOOLS) ]; \
	then \
		mkdir -p $(FBTFT_TOOLS_DIR); \
		cd $(FBTFT_TOOLS_DIR) && git clone $(FBTFT_TOOLS_URL) $(FBTFT_TOOLS); \
	fi

$(FBTFT_TOOLS)-build:
	@if [ -d $(FBTFT_TOOLS_DIR)/$(FBTFT_TOOLS) ]; \
	then \
		make --no-print-directory ads7846; \
		make --no-print-directory gpiok; \
		make --no-print-directory gpiom; \
		make --no-print-directory gpior; \
		make --no-print-directory stmpe; \
	fi

$(FBTFT_TOOLS)-clean:
	@if [ -d $(FBTFT_TOOLS_DIR)/$(FBTFT_TOOLS) ]; \
	then \
		make --no-print-directory ads7846-clean; \
		make --no-print-directory gpiok-clean; \
		make --no-print-directory gpiom-clean; \
		make --no-print-directory gpior-clean; \
		make --no-print-directory stmpe-clean; \
	fi

ads7846:
	@sed -i 's%^KDIR.*%KDIR := $(KERNEL_SRCDIR)%g' $(ADS7846_DIR)/Makefile
	@cd $(ADS7846_DIR) && PATH=$(KERNEL_PATH) make ARCH=$(ARCH) CROSS_COMPILE=$(CROSS_COMPILER) all
	@cd $(ADS7846_DIR) && \
		PATH=$(KERNEL_PATH) make ARCH=$(ARCH) CROSS_COMPILE=$(CROSS_COMPILER) \
		INSTALL_MOD_PATH=$(KERNEL_SRCDIR)/modules install

gpiob:
	@sed -i 's%^KDIR.*%KDIR := $(KERNEL_SRCDIR)%g' $(GPIOB_DIR)/Makefile
	@cd $(GPIOB_DIR) && PATH=$(KERNEL_PATH) make ARCH=$(ARCH) CROSS_COMPILE=$(CROSS_COMPILER) all
	@cd $(GPIOB_DIR) && \
		PATH=$(KERNEL_PATH) make ARCH=$(ARCH) CROSS_COMPILE=$(CROSS_COMPILER) \
		INSTALL_MOD_PATH=$(KERNEL_SRCDIR)/modules install

gpiok:
	@sed -i 's%^KDIR.*%KDIR := $(KERNEL_SRCDIR)%g' $(GPIOK_DIR)/Makefile
	@cd $(GPIOK_DIR) && PATH=$(KERNEL_PATH) make ARCH=$(ARCH) CROSS_COMPILE=$(CROSS_COMPILER) all
	@cd $(GPIOK_DIR) && \
		PATH=$(KERNEL_PATH) make ARCH=$(ARCH) CROSS_COMPILE=$(CROSS_COMPILER) \
		INSTALL_MOD_PATH=$(KERNEL_SRCDIR)/modules install

gpiom:
	@sed -i 's%^KDIR.*%KDIR := $(KERNEL_SRCDIR)%g' $(GPIOM_DIR)/Makefile
	@cd $(GPIOM_DIR) && PATH=$(KERNEL_PATH) make ARCH=$(ARCH) CROSS_COMPILE=$(CROSS_COMPILER) all
	@cd $(GPIOM_DIR) && \
		PATH=$(KERNEL_PATH) make ARCH=$(ARCH) CROSS_COMPILE=$(CROSS_COMPILER) \
		INSTALL_MOD_PATH=$(KERNEL_SRCDIR)/modules install

gpior:
	@sed -i 's%^KDIR.*%KDIR := $(KERNEL_SRCDIR)%g' $(GPIOR_DIR)/Makefile
	@cd $(GPIOR_DIR) && PATH=$(KERNEL_PATH) make ARCH=$(ARCH) CROSS_COMPILE=$(CROSS_COMPILER) all
	@cd $(GPIOR_DIR) && \
		PATH=$(KERNEL_PATH) make ARCH=$(ARCH) CROSS_COMPILE=$(CROSS_COMPILER) \
		INSTALL_MOD_PATH=$(KERNEL_SRCDIR)/modules install

stmpe:
	@sed -i 's%^KDIR.*%KDIR := $(KERNEL_SRCDIR)%g' $(STMPE_DIR)/Makefile
	@cd $(STMPE_DIR) && PATH=$(KERNEL_PATH) make ARCH=$(ARCH) CROSS_COMPILE=$(CROSS_COMPILER) all
	@cd $(STMPE_DIR) && \
		PATH=$(KERNEL_PATH) make ARCH=$(ARCH) CROSS_COMPILE=$(CROSS_COMPILER) \
		INSTALL_MOD_PATH=$(KERNEL_SRCDIR)/modules install

ads7846-clean:
	@cd $(ADS7846_DIR) && PATH=$(KERNEL_PATH) make ARCH=$(ARCH) CROSS_COMPILE=$(CROSS_COMPILER) clean

gpiob-clean:
	@cd $(GPIOB_DIR) && PATH=$(KERNEL_PATH) make ARCH=$(ARCH) CROSS_COMPILE=$(CROSS_COMPILER) clean

gpiok-clean:
	@cd $(GPIOK_DIR) && PATH=$(KERNEL_PATH) make ARCH=$(ARCH) CROSS_COMPILE=$(CROSS_COMPILER) clean

gpiom-clean:
	@cd $(GPIOM_DIR) && PATH=$(KERNEL_PATH) make ARCH=$(ARCH) CROSS_COMPILE=$(CROSS_COMPILER) clean

gpior-clean:
	@cd $(GPIOR_DIR) && PATH=$(KERNEL_PATH) make ARCH=$(ARCH) CROSS_COMPILE=$(CROSS_COMPILER) clean

stmpe-clean:
	@cd $(STMPE_DIR) && PATH=$(KERNEL_PATH) make ARCH=$(ARCH) CROSS_COMPILE=$(CROSS_COMPILER) clean
