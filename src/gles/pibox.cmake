
#
# CMake defines to cross-compile to ARM/Linux on BCM2708 using glibc.
#

# This needs to use a tag, so the PiBox build system can specify the prefix
SET(CMAKE_TCPREFIX [XCCPREFIX])

# The rest of this just works.
SET(CMAKE_SYSTEM_NAME Linux)
SET(CMAKE_C_COMPILER ${CMAKE_TCPREFIX}-gcc)
SET(CMAKE_CXX_COMPILER ${CMAKE_TCPREFIX}-g++)
SET(CMAKE_ASM_COMPILER ${CMAKE_TCPREFIX}-gcc)
SET(CMAKE_SYSTEM_PROCESSOR arm)

#ADD_DEFINITIONS("-march=[MARCH]")
add_definitions("-mcpu=[MCPU] -mfpu=[MFPU] -mfloat-abi=hard")

# rdynamic means the backtrace should work
IF (CMAKE_BUILD_TYPE MATCHES "Debug")
   add_definitions(-rdynamic)
ENDIF()

# avoids annoying and pointless warnings from gcc
SET(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -U_FORTIFY_SOURCE")

