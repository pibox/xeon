# Build System For Embedded Boards - builds
# cross compiler, kernel and filesystem.
# ----------------------------------------------------
all: defaultBB

# These files contain common variables and targets.
include config.mk
include util.mk

# ---------------------------------------------------------------
# Default build
# This should create a default distribution.  
# ---------------------------------------------------------------
defaultBB: $(XCC_T) $(FIRMWARE_T) $(GLES_T) $(KERNEL_T) $(BUILDROOT_T) $(BUSYBOX_T) $(PKG_T)

# ---------------------------------------------------------------
# Cleanup targets - seldom used since they affect all 
# components at once.
# ---------------------------------------------------------------
clean: 
	@for component in $(TARGETS); do \
		echo "Clobbering: $$component"; \
		make --no-print-directory $$component-clean; \
	done

# Careful - this wipes your archive out too!
clobber: 
	@for component in $(TARGETS); do \
		echo "Clobbering: $$component"; \
		if echo "$$component" | egrep -q "gles|pkg" ; then \
		    sudo make --no-print-directory $$component-clobber; \
		else \
		    make --no-print-directory $$component-clobber; \
		fi; \
	done
	@rm -rf $(ARCDIR) $(BLDDIR) 

shellcheck:
	@scripts/shellcheck.sh "`pwd`"
