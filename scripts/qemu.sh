#!/bin/bash
# https://blog.agchapman.com/using-qemu-to-emulate-a-raspberry-pi/
# https://azeria-labs.com/emulate-raspberry-pi-with-qemu/
# https://wiki.qemu.org/Documentation/Platforms/ARM
# https://gist.github.com/Liryna/10710751
# --------------------------------------------------------------------

# To create the disk image:
# ./mksd.sh -f pibox.img
# mkdir qemu
# mv pibox.img qemu
# ./mkinstall.sh -f qemu/pibox.img
# --------------------------------------------------------------------
# Other possible setups
# -net nic -net user \
# -net tap,ifname=vnet0,script=no,downscript=no \
# -append "quiet vga=current dwc_otg.lpm_enable=0 console=ttyAMA0,115200 console=tty1 root=/dev/mmcblk0p2 rootfstype=ext3 elevator=deadline rootwait logo.nologo dwc_otg.fiq_fsm_mask=0x3" \
# -append "quiet vga=current console=ttyAMA1,115200 console=tty1 root=/dev/mmcblk0p2 rootfstype=ext3 rootwait" \
# -serial stdio \

#--------------------------------------------------------------
# Initialization
#--------------------------------------------------------------
HW="rpi2"
STDIO=""
MACHINE=""
DTB=""
MEM=256
CORES=1
CPU=""
FIRSTBOOT=""
STEP=""
DEBUG=""
QEMU="qemu-system-arm"
KERNEL="kernel.img"
INITRAMFS="initramfs.gz"
RPI_FS="qemu/pibox.img"
APPEND="quiet vga=current console=ttyAMA0,115200 console=tty0 root=/dev/mmcblk0p2 rootfstype=ext3 rootwait"

#--------------------------------------------------------------
# Functions
#--------------------------------------------------------------
doHelp()
{
    echo ""
    echo "$0 [-fsv | -h <type> ]"
    echo "where"
    echo "-f           Stop at end of firstboot"
    echo "-s           Step through initramfs init script"
    echo "-d           Debug initramfs (quiet is default)"
    echo "-h <type>    Harware type: Default: ${HW}"
    echo "             Supported values: rpi (b-plus), rpi2, rpi3, zero, zerow"
    echo ""
    echo "RPi hardware types are very different under QEMU so you must"
    echo "specify which board type yout want to use."
    echo ""
    echo "Status:"
    echo "rpi  : No display."
    echo "rpi2 : Boots but no keyboard/mouse input."
    echo "rpi3 : Doesn't work with PiBox."
    echo "zero : No display."
    echo "zerow: No display."
    echo ""
}

#--------------------------------------------------------------
# Parse command line arguments
#--------------------------------------------------------------
while getopts ":dfsh:" Option
do
    case ${Option} in
    d) DEBUG="pdebug";;
    f) FIRSTBOOT="firstboot=noreboot";;
    s) STEP="pstep";;
    h) HW="${OPTARG}";;
    *) doHelp; exit 0;;
    esac
done

# Validate HW option.
case "${HW}" in
    rpi) 
        echo "Setting up for rpi"
        MACHINE="versatilepb"
        CPU="-cpu arm1176"
        DTB="-dtb firmware/bcm2708-rpi-b-plus.dtb"
        MEM=256
        CORES=1
        QEMU=~/bin/qemu-system-arm
        ;;

    zero*) 
        echo "Setting up for zero"
        MACHINE="versatilepb"
        # CPU="-cpu arm1176"
        [[ "${HW}" == "zero"  ]] && DTB="-dtb firmware/bcm2708-rpi-zero.dtb"
        [[ "${HW}" == "zerow" ]] && DTB="-dtb firmware/bcm2708-rpi-zero-w.dtb"
        MEM=256
        CORES=1
        QEMU=~/bin/qemu-system-arm
        ;;

    rpi2) 
        echo "Setting up for rpi2"
        MACHINE="raspi2"
        CPU="-cpu arm1176"
        CPU="-cpu cortex-a7"
        DTB="-dtb firmware/bcm2709-rpi-2-b.dtb"
        MEM=1024
        CORES=4
        QEMU=~/bin/qemu-system-arm
        QEMU=/usr/bin/qemu-system-arm
        # STDIO="-net user,hostfwd=tcp::5022-:22 -net nic"
        ;;

    rpi3) 
        echo "Setting up for rpi3"
        # RPi3 - currently doesn't work with PiBox build
        MACHINE="raspi3"
        DTB="-dtb firmware/bcm2710-rpi-3-b.dtb"
        MEM=1024
        CORES=4
        CPU=""
        QEMU="qemu-system-aarch64"
        ;;

    *)  echo "Invalid hardware specified (-h)."
        doHelp
        exit 0
        ;;
esac

#--------------------------------------------------------------
# Main
#--------------------------------------------------------------
APPEND="${APPEND} ${STEP} ${QUIET} ${FIRSTBOOT}"

set -x
${QEMU} \
    -M "${MACHINE}" ${CPU} \
    -m ${MEM} \
    -smp ${CORES} \
    -no-reboot \
    -kernel ${KERNEL} \
    -initrd ${INITRAMFS} \
    ${DTB} \
    ${STDIO} \
    -append "${APPEND}" \
    -drive "file=$RPI_FS,index=0,media=disk,format=raw"

    # This is used if we emulate the SD card.
    # -drive file=qemu/pibox.img,format=raw,if=sd


