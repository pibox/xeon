#!/bin/bash -p
# Format an SD card for use with PinePhone Pro
# See: https://wiki.pine64.org/wiki/PinePhone_Pro_Development#Levinboot_Based_Kernel_Development_Image
# Note: this script is specific to using Levinboot.
# --------------------------------------------

#--------------------------------------------------------------
# Initialization
#--------------------------------------------------------------
MODE="l"
DOFILE=0
DEVICE=
IMGFILE="pibox.img"
GUID=()
DEVPATH="/dev/disk/by-partlabel/"

BOOTFSPARTNAME="sd-lboot"
PAYLOADPARTNAME="sd-lpayload"
ROOTFSPARTNAME="sd-rootfs"

UBOOTLOADERPARTNAME="idbloader"
UBOOTPARTNAME="uboot"
UBOOTBOOTPARTNAME="boot"
UBOOTROOTFSPARTNAME="rootfs"
UBOOTDATAPARTNAME="data"

#--------------------------------------------------------------
# Functions
#--------------------------------------------------------------
msg()
{
    echo ""
    echo "### $1"
}

die()
{
    msg "Failure: $*"
    doHelp
    exit 1
}

#--------------------------------------------------------------
# Provide command line usage assistance
#--------------------------------------------------------------
doHelp()
{
    echo ""
    echo "$0 [-?| -d <device> | -m <mode>]"
    echo "where"
    echo "-d <device>    Device to partition."
    echo "-m <mode>      Mode; Default: ${MODE}"
    echo "               One of: l (levinboot), u (uboot)"
    echo ""
}

#--------------------------------------------------------------
# Levinboot processing functions
#--------------------------------------------------------------
# Create the partitions on the SD card.
makeLevinbootSD()
{
    # These values are required by Levinboot as "type" fields, not UUIDs!
    # See: https://gitlab.com/DeltaGem/levinboot (section: Booting from SD/eMMC/NVMe)
    GUID+=( D7B1F817-AA75-2F4F-830D-84818A145370 )
    GUID+=( E5AB07A0-8E5E-46F6-9CE8-41A518929B7C )
    GUID+=( 5f04b556-c920-4b6d-bd77-804efe6fae01 )
    GUID+=( c195cc59-d766-4b78-813f-a0e1519099d8 )

    local TMPFILE
    TMPFILE="$(mktemp -t upgrade.XXXXX)"
    cat <<EOF> "${TMPFILE}"
label: gpt
first-lba: 64
table-length: 8
start=64, size=8128, type=${GUID[0]}, name="${BOOTFSPARTNAME}"
size=60M, type=${GUID[1]}, name="${PAYLOADPARTNAME}1"
size=60M, type=${GUID[2]}, name="${PAYLOADPARTNAME}2"
size=60M, type=${GUID[3]}, name="${PAYLOADPARTNAME}3"
size=14G name="${ROOTFSPARTNAME}1"
name="${ROOTFSPARTNAME}2"
EOF
    cat ${TMPFILE}
    set -x
    sudo sfdisk ${DEVICE} < "${TMPFILE}"
    set +x
    rm -f "${TMPFILE}"
}

# Wait for partitions to show up on host system.
waitForLevinbootPartitions()
{
    local cnt
    cnt=0
    okay=0

    while [[ ${cnt} -lt 60 ]]
    do
        [[ -b "${DEVPATH}${ROOTFSPARTNAME}1" ]] && okay=1 && break
        sleep 1
        cnt=$(( $cnt + 1 ))
    done
    [[ ${okay} -eq 1 ]] || die "Can't find parition device nodes on host after partioning device!"
}

# Format the rootfs partitions
doFormatLevinbootPartitions()
{
    sudo mkfs.ext3 -F -F -L "${ROOTFSPARTNAME}1" "${DEVPATH}${ROOTFSPARTNAME}1"
    sudo mkfs.ext3 -F -F -L "${ROOTFSPARTNAME}2" "${DEVPATH}${ROOTFSPARTNAME}2"
}

# Driver function for Levinboot-based SD cards.
doLevinboot()
{
    # Create partitions
    makeLevinbootSD
    waitForLevinbootPartitions

    # Create filesystems on rootfs partitions
    doFormatLevinbootPartitions
}

#--------------------------------------------------------------
# u-boot processing functions
#--------------------------------------------------------------
# Create the partitions on the SD card.
makeUbootSD()
{
    local BGUID

    # GPT says this "type" field is actually "Microsoft Basic data", 
    # which is the required partition type on the boot partition.
    BGUID="EBD0A0A2-B9E5-4433-87C0-68B6B72699C7"

    # There could have been 5 partitions, but we're only using one GUID -
    # for the rootfs partition.
    GUID+=( $(uuidgen) )
    GUID+=( $(uuidgen) )
    GUID+=( $(uuidgen) )
    GUID+=( $(uuidgen) )
    GUID+=( $(uuidgen) )

    # These two partitions would normally go at the start, but the u-boot we're using
    # wants the first partition to be the boot partition, so these areas of the SD card
    # will not be partions, just blank space we write bootloaders to.
    # start=64, size=16320, uuid=${GUID[0]}, name="${UBOOTLOADERPARTNAME}"
    # start=16384, size=8192, uuid=${GUID[1]}, name="${UBOOTPARTNAME}"
    # start=32768, size=229376, type=${BGUID}, name="${UBOOTBOOTPARTNAME}", bootable
    # size=14G, uuid=${GUID[3]}, name="${UBOOTROOTFSPARTNAME}"
    # uuid=${GUID[4]}, name="${UBOOTDATAPARTNAME}"

    local TMPFILE
    TMPFILE="$(mktemp -t upgrade.XXXXX)"
    cat <<EOF> "${TMPFILE}"
label: gpt
first-lba: 64
table-length: 8
start=32768, size=1G, type=${BGUID}, name="${UBOOTBOOTPARTNAME}", bootable
start=2129920, size=14G, uuid=${GUID[3]}, name="${UBOOTROOTFSPARTNAME}"
uuid=${GUID[4]}, name="${UBOOTDATAPARTNAME}"
EOF
    cat ${TMPFILE}
    set -x
    sudo sfdisk ${DEVICE} < "${TMPFILE}"
    set +x
    rm -f "${TMPFILE}"
}

# Wait for partitions to show up on host system.
waitForUbootPartitions()
{
    local cnt
    cnt=0
    okay=0

    while [[ ${cnt} -lt 60 ]]
    do
        [[ -b "${DEVPATH}${UBOOTROOTFSPARTNAME}" ]] && okay=1 && break
        sleep 1
        cnt=$(( $cnt + 1 ))
    done
    [[ ${okay} -eq 1 ]] || die "Can't find parition device nodes on host after partioning device!"
}

# Format the rootfs partitions
doFormatUbootPartitions()
{
    sudo mkfs.vfat -n "${UBOOTBOOTPARTNAME}" "${DEVPATH}${UBOOTBOOTPARTNAME}"
    sudo mkfs.ext3 -F -F -L "${UBOOTROOTFSPARTNAME}" "${DEVPATH}${UBOOTROOTFSPARTNAME}"
    sudo mkfs.ext3 -F -F -L "${UBOOTDATAPARTNAME}" "${DEVPATH}${UBOOTDATAPARTNAME}"
}

# Driver function for Levinboot-based SD cards.
doUboot()
{
    # Create partitions
    makeUbootSD
    waitForUbootPartitions

    # Create filesystems on rootfs partitions
    doFormatUbootPartitions
}

#--------------------------------------------------------------
# Common functions
#--------------------------------------------------------------

#--------------------------------------------------------------
# Read command line arguments
#--------------------------------------------------------------
while getopts ":d:m:" Option
do
    case $Option in
    d) DEVICE="${OPTARG}";;
    m) MODE="${OPTARG}";;
    *) doHelp; exit 0;;
    esac
done

#--------------------------------------------------------------
# Error checking
#--------------------------------------------------------------
[[ -n "${DEVICE}" ]] || die "You must supply the SD device on the command line (-d option)."
[[ -b "${DEVICE}" ]] || die "${DEVICE} does not exist or is not a block device."

case "${MODE}" in
    "l") MODENAME="Levinboot";;
    "u") MODENAME="u-boot";;
    "*") die "Invalid mode."
esac

#--------------------------------------------------------------
# Main
#--------------------------------------------------------------
msg "About to format ${DEVICE} for use with ${MODENAME}. Use Ctrl-C to abort."
sleep 3

# Clear the device of previous configurations
sudo wipefs --all ${DEVICE}

# Format SD card.
[[ "${MODE}" == "l" ]] && doLevinboot
[[ "${MODE}" == "u" ]] && doUboot

