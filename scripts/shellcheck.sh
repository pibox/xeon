#!/bin/bash
# Load the config file and test each script.
# Any failures are fatal.
# -----------------------------------------------
SHELLCHECK="shellcheck"
CONFIG="configs/shellcheck.cfg"
TOPDIR="${1}"

echo "TOPDIR: ${TOPDIR}"

if ! which "${SHELLCHECK}" > /dev/null 2>&1; then
	echo "Can't find shellcheck.  Is it installed?"
	exit 1
fi

while read line; do
    if echo "${line}" | grep "^#" >/dev/null 2>&1; then
        continue
    fi
	if ! "${SHELLCHECK}" -s ${line}; then
        SCRIPT="$(echo "${line}" | cut -f2- -d" ")"
        echo "Failed shellcheck: ${SCRIPT}"
        exit 1
    fi
done < "${TOPDIR}/${CONFIG}" 
