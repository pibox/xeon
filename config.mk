#----------------------------------------------------------------------
# Common Config variables and targets.
# Includes configurations from configs directory.
#----------------------------------------------------------------------

#---------------------------------------------------------------------
# Edit these for a new build environment

# Project Architecture and Vendor
# Xeon is based on PinePhone Pro hardware, which is an RK3399.
HW 			:= rk3399
ARCH        := arm64
XEON        := 1
PLATFORM    := Xeon
PROJECT     := xeon

# Build Version ID
BLD_VERSION := $(shell cat version.txt)
BLD_DATE := $(shell date)

#---------------------------------------------------------------------
# End of Build Configurable options
#---------------------------------------------------------------------
# Specify a SourceForge mirror
# You can override this by specifying it in your environment.
ifndef SF_MIRROR
SF_MIRROR = voxel
endif

# Number of parallel jobs.  Override this on the command line.
JOBS = $(shell grep processor /proc/cpuinfo | wc -l)

# Who am I?
UID = $(shell id -u)

# What kind of packaging do we need on this host?
# We want to catch errors when running external commands because some systems can actually
# have support for both rpm and dpkg.
define TEST_FOR_DPKG
$(shell
	/usr/bin/dpkg --search /usr/bin/dpkg 2>&1 || echo "No such file"
)
endef
define TEST_FOR_RPM
$(shell
	/usr/bin/rpm -q -f /usr/bin/rpm 2>&1 || echo "No such file"
)
endef
define TEST_FOR_PKG
$(shell if [ -z "$(1)" ]; then
	echo "Yes";
else
	echo "No";
fi;
)
endef
DORPM := $(call TEST_FOR_PKG,$(findstring No such file,$(call TEST_FOR_RPM)))
DODEB := $(call TEST_FOR_PKG,$(findstring No such file,$(call TEST_FOR_DPKG)))

#---------------------------------------------------------------------
# Target names - these are also used with a "." prefix as
# empty target files for various build sections.
# TARGETS gets updated in each component's .cfg file.

INIT_T          := init
TARGETS         = 
PKG_TARGETS     = 

# Where we can find the opkg-build utility is set with OPKG
ifeq ($(OPKG),)
OPKG_DIR            = /usr/local/bin
else
OPKG_DIR            = $(OPKG)
endif

#---------------------------------------------------------------------
# Directoriee: 
# The buld and archive downloads are kept in parallel directories from
# the source tree so hg status won't get confused by all the new files.
TOPDIR              := $(shell pwd)
SRCDIR              := $(TOPDIR)/src
ARCDIR              := $(TOPDIR)/../archive
BLDDIR              := $(TOPDIR)/../bld
PKGDIR              := $(TOPDIR)/../pkg
SCRIPTDIR           := $(TOPDIR)/scripts

#---------------------------------------------------------------------
# Include the configs directory files after the common configs
# Note: Order here is important

include configs/xcc.cfg
include configs/bootloader.cfg
include configs/kernel.cfg
include configs/busybox.cfg
include configs/buildroot.cfg
include configs/pkg.cfg
include configs/opkg.cfg

#---------------------------------------------------------------------
# Include the component makefiles
# Note: Order here is important
include configs/xcc.mk
include configs/kernel.mk
include configs/busybox.mk
include configs/buildroot.mk
include configs/buildroot-$(PROJECT).mk
include configs/pkg.mk
include configs/opkg.mk

#---------------------------------------------------------------------
# Config display target
showconfig:
	@$(MSG3) Common Configuration $(EMSG)
	@echo "Components           :$(TARGETS)"
	@echo "Packaged Components  :$(PKG_TARGETS)"
	@echo "Version              : $(BLD_VERSION)"
	@echo "ARCH                 : $(ARCH)"
	@echo "HW                   : $(HW)"
	@echo "PLATFORM             : $(PLATFORM)"
	@echo "PROJECT              : $(PROJECT)"
	@echo "XEON                 : $(XEON)"
	@echo "JOBS                 : $(JOBS)"
	@echo "SRCDIR               : $(SRCDIR)"
	@echo "ARCDIR               : $(ARCDIR)"
	@echo "BLDDIR               : $(BLDDIR)"
	@echo "CROSS_COMPILER       : $(CROSS_COMPILER)"
	@echo "XCC_PREFIXDIR        : $(XCC_PREFIXDIR)"
	@echo "XCC_PREFIX           : $(XCC_PREFIX)"
	@echo "KSRC                 : $(KSRC)"
	@echo "Kernel config (KSRC) : kernel-$(KSRC)"

